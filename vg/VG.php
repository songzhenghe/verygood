<?php
class VG{
    public static $config;
    public static $db;
	public static $bigmodule;
    public static $default_route='front/index/index/index';
	public static $request_method;
	public static $is_get;
	public static $is_post;
	public static $is_put;
	public static $is_delete;
	public static $is_ajax;
    public static $current_route;
    public static $uri;
    public static $request_uri;
    public static $ctl_file;
    public static $view_file;
    public static $view_dir;
    public static $pjt;
    public static $module;
    public static $control;
    public static $action;
	public static $get_var;
	public static $post_var;
	public static $gp_var;
	public static $forward;
	public static $project_debug=true;
	public static $classes;
	
    public static function error_404($msg=''){
        header("HTTP/1.1 404 Not Found");
		header("Status: 404 Not Found");
		$heading='404 Not Found';
		include(BASEPATH.'error_404.php');
        //header("Location: 404.html?msg={$msg}");
        exit;
    }
	public static function get($v=''){
		if($v){
			return isset(self::$get_var[$v])?self::$get_var[$v]:'';
		}
		return self::$get_var;
	}
	public static function post($v=''){
		if($v){
			return isset(self::$post_var[$v])?self::$post_var[$v]:'';
		}
		return self::$post_var;
	}
	public static function get_post($v=''){
		if($v){
			return isset(self::$gp_var[$v])?self::$gp_var[$v]:'';
		}
		return self::$gp_var;
	}
    public static function run(){
        $route=isset($_GET['r'])?$_GET['r']:self::$default_route;
        $route=trim($route);
        $route=trim($route,'/');
        if(substr_count($route,'/')<3){
            self::error_404('router_wrong_format');
        }
        $route=preg_replace('/\/{1,}/','/',$route);
        if(!preg_match('/^[A-Za-z_0-9\/]+$/',$route)){
            self::error_404('router_wrong_format');
        }
		self::$request_method=$_SERVER['REQUEST_METHOD'];
		self::$is_get=(self::$request_method=='GET')?TRUE:FALSE;
		self::$is_post=(self::$request_method=='POST')?TRUE:FALSE;
		self::$is_put=(self::$request_method=='PUT')?TRUE:FALSE;
		self::$is_delete=(self::$request_method=='DELETE')?TRUE:FALSE;
		self::$is_ajax=((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ) ? TRUE : FALSE;
        self::$current_route=$route;
        self::$uri=explode('/',self::$current_route);
        self::$request_uri=$_SERVER['REQUEST_URI'];
        
        
        self::$pjt=self::$uri[0];
        self::$module=self::$uri[1];
        self::$control=$control=self::$uri[2];
        self::$action=$action=self::$uri[3];
        
        $ctl_file=APPPATH.'controllers/'.self::$pjt.'/'.self::$module.'/'.self::$control.'Controller.class.php';
        $view_file=APPPATH.'views/'.$route.'.php';
        self::$ctl_file=$ctl_file;
        self::$view_file=$view_file;
		self::$view_dir=dirname($view_file).'/';
		
		include(BASEPATH.'functions.inc.php');
		
		define('GPC',get_magic_quotes_gpc());
		if(GPC){
			$_GET=stripslashes_array($_GET);
			$_POST=stripslashes_array($_POST);
		}
		
		// if (!self::is_php('5.3')){
			// @set_magic_quotes_runtime(0);
		// }
		
		self::$get_var=$_GET;
		self::$post_var=$_POST;
		self::$gp_var=array_merge($_GET,$_POST);
		
        $forward=self::get_post('forward');
		
        if($forward){
            self::$forward=urldecode($forward);
        }else{
            self::$forward=isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        }
		
        if(file_exists($ctl_file)){
            self::$config=include(APPPATH.'config/config.php');
            $db=include(APPPATH.'config/database.php');
            self::$db=$db[$active_group];
            $charset=self::$config['charset'];
			self::$bigmodule=include(APPPATH.'config/bigmodule.php');
            header("Content-Type:text/html;charset={$charset}");
            

            @set_time_limit(300);
            
            date_default_timezone_set(self::$config['time_reference']);
            self::session(isset($_POST['PHPSESSID'])?$_POST['PHPSESSID']:'');
            
            spl_autoload_register(array('VG','vg_autoload'));
            
            
            include(APPPATH.'functions.inc.php');
            
            include(APPPATH.'controllers/'.self::$uri[0].'/project.inc.php');
           
            include($ctl_file);
            
            $_control=$control.'Controller';
            if(!class_exists($_control)) self::error_404('control_not_found');
            
            $instance = new $_control();
            if(!method_exists($instance, $action)) self::error_404('action_not_found');
            $ref=new ReflectionMethod($instance,$action);
            $deny_list=array('__construct','__destruct','__call','__callStatic','__get','__set','__isset','__unset','__sleep','__wakeup','__toString','__invoke','__set_state','__clone');
            if($ref->isPublic() && !in_array($action,$deny_list)){
				if(DEBUG && !self::$is_ajax && !file_exists(self::$view_dir)) @mkdir(self::$view_dir,0777,true);
				$instance->$action(self::$view_file,self::$view_dir);
			}else{
				self::error_404();
			}
			
			if(defined('DEBUG') and DEBUG==TRUE and self::$project_debug){
				debug::stop();
				debug::message();
			}
			
            return true;
        }else{
            self::error_404('ctl_file_not_found');
        }
        
    }
	public static function is_php($version = '5.0.0'){
		return (version_compare(PHP_VERSION, $version) < 0) ? FALSE : TRUE;
	}
    public static function session($id=''){
        session_set_cookie_params(0,self::$config['cookie_path'],self::$config['cookie_domain']);
        if($id) session_id($id);
        session_start();
    }
    public static function &get_instance(){
        return Controller::get_instance();
    }
    public static function vg_autoload($name){
        if(file_exists(BASEPATH.$name.'.class.php')){
            include(BASEPATH.$name.'.class.php');
        }
    }
	public static function s_i_p($path_array){
		array_unshift($path_array,get_include_path());
		set_include_path(implode(PATH_SEPARATOR,$path_array));
	}
	public static function load($class,$path='',$sign='def'){
		if(isset(self::$classes[$sign][$class])) return self::$classes[$sign][$class];
		if(strpos($class,'Model')!==false){
			self::$classes[$sign][$class]=load_model($class,$path);
		}else if(strpos($class,'Service')!==false){
			self::$classes[$sign][$class]=load_service($class,$path);
		}else{
			self::$classes[$sign][$class]=new $class();
		}
		return self::$classes[$sign][$class];
	}
}
