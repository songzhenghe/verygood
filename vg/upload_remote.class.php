<?php
class upload_remote{
	protected $ftp=null;
	
	public function __construct(){
		if(VG::$config['ftp_remote'] && VG::$config['remote_url']) {
			$this->ftp = new dftp(VG::$config['ftp_host'], VG::$config['ftp_user'], VG::$config['ftp_pass'], VG::$config['ftp_port'], VG::$config['ftp_path'], VG::$config['ftp_pasv'], VG::$config['ftp_ssl']);
		}
	}
	public function is_connected(){
		return isset($this->ftp->connected)?$this->ftp->connected:0;
	}
	
	public function add($saveto){
		$saveto2=VG::$config['base_url'].$saveto;
		if(VG::$config['ftp_remote'] && VG::$config['remote_url']) {
			if($this->ftp->connected) {
				$exp = explode("common/uploads/", $saveto);
				$remote = $exp[1];
				if($this->ftp->dftp_put($saveto, $remote)) {
					if(!VG::$config['ftp_save'] && file_exists('./'.$saveto) && (strpos('./'.$saveto,'..')===false)){
						unlink('./'.$saveto);
					}
					$saveto2 = VG::$config['remote_url'].$remote;
				}
			}
		}
		return $saveto2;
	}
	public function del($file){
		if(VG::$config['ftp_remote'] && VG::$config['remote_url']) {
			if($this->ftp->connected) {
				if(strpos($file, VG::$config['remote_url']) !== false) {
					$file = str_replace(VG::$config['remote_url'], '', $file);
					$this->ftp->dftp_delete($file);
					if(VG::$config['ftp_save'] && file_exists('./common/uploads/'.$file) && (strpos('./common/uploads/'.$file,'..')===false)){
						unlink('./common/uploads/'.$file);
					}
				}
			}
		}
		return true;
	}
	
}