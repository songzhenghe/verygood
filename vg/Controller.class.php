<?php
class Controller{
    private static $instance;
	
	protected $conf=null;
    public $db=null;
    
	public function __construct(){
        self::$instance =& $this;
		$this->conf=VG::$config;
        $model=new Model();
		$this->db=$model->getConnection();
	}
    public static function &get_instance(){
		return self::$instance;
	}

//
}