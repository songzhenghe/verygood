<?php
class ModelB extends Model{
	public function findByPk($pk){
		return $this->easy_find($pk);
    }
	public function updateByPk($pk,$data){
		return $this->where(array("{$this->primaryKey}"=>$pk))->easy_update($data,0,false,false);
    }
	public function deleteByPk($pk){
		return $this->easy_delete($pk);
    }
}