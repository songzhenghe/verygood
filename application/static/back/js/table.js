// JavaScript Document
var table = {};
table.init = function () {
	table.line();	
	$("table tr").bind("mouseover", table.tr_mouseover); //表格TR移动上去效果
	$("table tr").bind("mouseout", table.tr_mouseout); //表格TR移开效果
	$(".input").bind("focus", table.input_focus); //表单选中效果
	$(".input").bind("blur", table.input_blur); //表单移开效果
	$("table tr td textarea").bind("focus", table.input_focus); //表单选中效果
	$("table tr td textarea").bind("blur", table.input_blur); //表单移开效

	table.html();
}
/* 初始化表格 表格隔行变色*/
table.line = function () {
	$("table").each(function(j) {
		$("table").eq(j).find('tr').each(function(i) {	
			if ((i%2) == 0) {
				//$(this).addClass('tbalt');
			} else {
				$(this).find('th').css('background', '#FFFFFF');	
			}
		})
	});
}

table.tr_mouseover = function () {
	$(this).addClass('tr_mouseover');
}

table.tr_mouseout = function () {
	$(this).removeClass('tr_mouseover');
}

table.input_focus = function () {
	$(this).addClass('input_focus');	
}

table.input_blur = function () {
	$(this).removeClass('input_focus');	
}

table.html = function () {
	
}
$(document).ready(function(){
	table.init();	
});