<?php
//系统配置类
class core_conf extends Controller {
	protected $SETTING=array();
	protected $MODULE=array();
	
	public function __construct(){
		parent::__construct();
		$this->SETTING['abc']='123';
		$this->MODULE=array(
			
			'1'=>array('moduleid'=>1,'module_en'=>'admin','module_cn'=>'网站设置','module_dir'=>'back/admin','linkurl'=>''),
			'2'=>array('moduleid'=>2,'module_en'=>'member','module_cn'=>'会员','module_dir'=>'back/member','linkurl'=>''),
			'8'=>array('moduleid'=>8,'module_en'=>'product','module_cn'=>'产品','module_dir'=>'back/product','linkurl'=>''),
			'9'=>array('moduleid'=>9,'module_en'=>'log','module_cn'=>'日志','module_dir'=>'back/log','linkurl'=>''),
		);
	}
	public function module_list(){
		return $this->MODULE;
	}
	public function mod_single($moduleid){
		return $this->MODULE[$moduleid];
	}
	public function mod_menu($moduleid){
		$file=APPPATH.'controllers/'.$this->MODULE[$moduleid]['module_dir'].'/fathers/module_conf_'.$moduleid.'Controller.class.php';
		if(!file_exists($file)) return array();
		include_once($file);
		$class="module_conf_{$moduleid}Controller";
		return call_user_func_array(array($class,'_menu'),array());
	}
	public function mod_auth($moduleid){
		$file=APPPATH.'controllers/'.$this->MODULE[$moduleid]['module_dir'].'/fathers/module_conf_'.$moduleid.'Controller.class.php';
		include_once($file);
		$file=APPPATH.'controllers/'.$this->MODULE[$moduleid]['module_dir'].'/fathers/module_auth_'.$moduleid.'Controller.class.php';
		if(!file_exists($file)) return array();
		include_once($file);
		$class="module_auth_{$moduleid}Controller";
		return call_user_func_array(array($class,'_auth'),array());
	}
	public function mod_category($moduleid){
		$file=APPPATH.'controllers/'.$this->MODULE[$moduleid]['module_dir'].'/fathers/module_conf_'.$moduleid.'Controller.class.php';
		include_once($file);
		$file=APPPATH.'controllers/'.$this->MODULE[$moduleid]['module_dir'].'/helpers/'.$this->MODULE[$moduleid]['module_en'].'Helper.class.php';
		if(!file_exists($file)) return array(false,array());
		include_once($file);
		$class=$this->MODULE[$moduleid]['module_en'].'Helper';
		$obj=new $class;
		if($obj->category==false) return array(false,array());
		return array($obj->category,$obj->category());
	}
}