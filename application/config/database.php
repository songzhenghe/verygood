<?php
$active_group = 'default';
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = 'root';
$db['default']['database'] = 'verygood';
$db['default']['dbprefix'] = 'vg_';
$db['default']['db_debug'] = TRUE;
$db['default']['char_set'] = 'utf8';
$db['default']['engine'] = 'pdoMysql_easy';
// $db['default']['engine'] = 'pdoMysql_dr';

$db['default']['dbprefix_more']=array('fv_');

$db['default']['dsn']='mysql:host=localhost;dbname=verygood;port=3306';
return $db;