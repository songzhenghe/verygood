<?php
$config['base_url']	= auto_url();
$config['charset'] = 'UTF-8';
$config['encryption_key'] = '';
$config['cookie_prefix']	= "";
$config['cookie_domain']	= ".localhost.com";
$config['cookie_path']		= "/";
$config['cookie_secure']	= FALSE;
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;
$config['time_reference'] = 'Asia/Shanghai';


$config['ftp_remote'] = '0';//远程FTP文件上传 1开启，0关闭
$config['ftp_host'] = '127.0.0.1';//FTP主机 可以是 FTP 服务器的 IP 地址或域名
$config['ftp_port'] = '21';//FTP端口
$config['ftp_user'] = 'anonymous';//FTP账号 该帐号必需具有以下权限：读取文件、写入文件、删除文件、创建目录、子目录继承
$config['ftp_pass'] = '';//FTP密码
$config['ftp_ssl'] = '0';//SSL连接 FTP 服务器必需开启了 SSL 才可以启用
$config['ftp_pasv'] = '0';//被动模式(PASV)连接 一般情况下非被动模式即可，如果存在上传失败问题，可尝试打开此设置
$config['ftp_save'] = '0';//保留源文件 如果选择是，上传到本服务器上的文件在上传到FTP服务器后，不自动删除，相当于在本服务器多了一份备份，文件保存于common/uploads/目录
$config['ftp_path'] = '/';//远程存储目录 例如 /wwwroot/img/ 或者 /httpdocs/img/具体以实际情况为准
$config['remote_url'] = 'http://www.localhost.com/111/';//远程访问URL 例如 http://static.destoon.com/，注意以 / 结尾


$config['mail_host']='smtp.126.com';
$config['mail_port']='25';
$config['mail_from']='songzhenghe89@126.com';
$config['mail_password']='';

//$config['memServers'] = array("127.0.0.1", 11211);	     //使用memcache服务器
/*
如果有多台memcache服务器可以使用二维数组
$config['memServers'] = array(
		array("www.a.net", '11211'),
		array("www.b.com", '11211'),
		...
	);
*/

return $config;