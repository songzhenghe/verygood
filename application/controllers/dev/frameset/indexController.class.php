<?php
class indexController extends accessController{
    public function ajax_0(){
		VG::$project_debug=false;
        echo time();
    }
    public function index($tpl){
        include $tpl;
    }
    public function left($tpl){
		VG::$project_debug=false;
        include $tpl;
    }
    public function main($tpl){
		$test_service=load_service('test');
        include $tpl;
    }
    public function top($tpl){
		VG::$project_debug=false;
        include $tpl;
    }
	public function sys_info($tpl){
		$test_service=load_service('test');
		include $tpl;
	}
	public function phpinfo(){
		phpinfo();
	}
    public function logout(){
        unset($_SESSION['MY_DEV']);
        
        $_SESSION=array();
        if(isset($_COOKIE[session_name()])){
            setcookie(session_name(),'',time()-3600,'/');
        }
        session_destroy();
        $u=url('dev/welcome/index/login');
        echo "<script>window.parent.location.href='{$u}';</script>";
        exit;
    }
}