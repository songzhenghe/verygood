<?php
//模块验证
class module_auth_MODULEID_OF_MINEController extends module_conf_MODULEID_OF_MINEController{
	protected static $auth_ctl;
	protected static $auth_act;
	protected $module_category;
    public function __construct(){
        parent::__construct();
		
		if(!$this->priv()) msg(0,'没有权限');
		
		$this->_init();

    }
	public static function _auth(){
		self::$auth_ctl=array(
			'index'=>array('name'=>'index','chinese'=>'MODULE_CN_OF_MINE','is_auth'=>1,'note'=>''),

			
		);
		self::$auth_act=array(
			'index'=>array(
				'setting'=>array('name'=>'setting','chinese'=>'模块设置','is_auth'=>1,'note'=>''),
				'quick_menu'=>array('name'=>'quick_menu','chinese'=>'菜单聚合','is_auth'=>0,'note'=>''),
			),

			
			
		);
		return array(self::$auth_ctl,self::$auth_act);
	}
	protected function _init(){
		
		if($this->groupid==1){
			$this->module_category='*';
		}else{
			$cat_back_model=load_model('cat_back');
			$category=$cat_back_model->field('id,catid')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid))->easy_find();
			$this->module_category=$category?explode(',',$category['catid']):array(0);
		}
	}
	protected function priv(){
		
		$this->auth_is_valid(self::_auth());
		
		$flag=$this->open_for_everyone(self::_auth());
		if($flag==true) return true;
		
		$flag=parent::priv();
		if($flag==true) return true;
		
		
		$access_back_model=load_model('access_back');
		$data=$access_back_model->field('id')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid,'module'=>VG::$module,'controller'=>VG::$control,'action'=>VG::$action))->easy_find();
		if($data['id']>0) return true;
		
		
		return false;
		
	}
	protected function check_catid($catid){
		if($this->module_category=='*') return true;
		if(!in_array($catid,$this->module_category)) msg(0,'没有权限');
		return true;
	}
	protected function check_itemid($itemid){
		$catid=0;//取得catid
		return $this->check_catid($catid);
	}
	//检查有没有该客户的权限
	protected function check_customer($customer_id){
		if($this->groupid==1) return true;
		if(!$customer_id) return false;
		if(is_array($customer_id)){
			$c=$this->db->mselect("select customer_id from __member_customer__ where userid='{$this->userid}'");
			$c=multi2single_min($c,'customer_id');
			return array_intersect($customer_id,$c)==count($customer_id);
		}else{
			$r=$this->db->select("select itemid from __member_customer__ where userid='{$this->userid}' and customer_id='{$customer_id}'");
			return $r['itemid']>0;
		}
	}
	
	
}