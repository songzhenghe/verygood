<?php
//模块配置
class module_conf_MODULEID_OF_MINEController extends accessController{
	protected $setting=array();
	protected static $menu;
	public $category=false;
    public function __construct(){
        parent::__construct();
		
		list($this->moduleid)=sscanf(__CLASS__,'module_conf_%dController');
		$this->module_en='MODULE_EN_OF_MINE';
		$this->module_cn='MODULE_CN_OF_MINE';
		$this->linkurl='?r=back/MODULE_EN_OF_MINE/index/index';
		
		$setting_file=APPPATH.'setting/'.$this->moduleid.'.php';
		if(!file_exists($setting_file)){
			// msg(0,'配置文件不存在');
			copy(APPPATH.'setting/0.php',APPPATH.'setting/'.$this->moduleid.'.php');
		}
		$this->setting=(array) include($setting_file);
		
		defined('MD_ROOT') or define('MD_ROOT',APPPATH.'controllers/back/MODULE_EN_OF_MINE/');
		
    }
	public static function _menu(){
		self::$menu=array(
			
			array('模块设置','?r=back/MODULE_EN_OF_MINE/index/setting','main',1),
			

		);
		return self::$menu;
	}
	
}