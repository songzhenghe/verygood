<?php
class indexController extends accessController{
    public function index(){
        
    }
	
	public function create_module($tpl){
		extract(VG::$gp_var);
		if($_POST){
			// $dir='back/';
			// $moduleid=100;
			// $module_en='demo';
			// $module_cn='demo';
			
			if(!$dir or !$moduleid or !$module_en or !$module_cn){
				msg(0);
			}
			
			if(file_exists(APPPATH.'controllers/'.$dir.$module_en)){
				msg(0,'模块目录已经存在');
			}
			
			$this->dir_copy(APPPATH.'controllers/dev/sample/',APPPATH.'controllers/'.$dir.$module_en);
			
			$ct=file_get_contents(APPPATH.'controllers/'.$dir.$module_en.'/indexController.class.php');
			file_put_contents(APPPATH.'controllers/'.$dir.$module_en.'/indexController.class.php',str_replace(array('MODULEID_OF_MINE'),array($moduleid),$ct));
			
			$ct=file_get_contents(APPPATH.'controllers/'.$dir.$module_en.'/fathers/module_auth_MODULEID_OF_MINEController.class.php');
			file_put_contents(APPPATH.'controllers/'.$dir.$module_en.'/fathers/module_auth_MODULEID_OF_MINEController.class.php',str_replace(array('MODULEID_OF_MINE','MODULE_EN_OF_MINE','MODULE_CN_OF_MINE'),array($moduleid,$module_en,$module_cn),$ct));
			rename(APPPATH.'controllers/'.$dir.$module_en.'/fathers/module_auth_MODULEID_OF_MINEController.class.php',APPPATH.'controllers/'.$dir.$module_en."/fathers/module_auth_{$moduleid}Controller.class.php");
			
			$ct=file_get_contents(APPPATH.'controllers/'.$dir.$module_en.'/fathers/module_conf_MODULEID_OF_MINEController.class.php');
			file_put_contents(APPPATH.'controllers/'.$dir.$module_en.'/fathers/module_conf_MODULEID_OF_MINEController.class.php',str_replace(array('MODULEID_OF_MINE','MODULE_EN_OF_MINE','MODULE_CN_OF_MINE'),array($moduleid,$module_en,$module_cn),$ct));
			rename(APPPATH.'controllers/'.$dir.$module_en.'/fathers/module_conf_MODULEID_OF_MINEController.class.php',APPPATH.'controllers/'.$dir.$module_en."/fathers/module_conf_{$moduleid}Controller.class.php");
			
			
			
			$this->dir_copy(APPPATH.'views/dev/sample/',APPPATH.'views/'.$dir.$module_en);
			
			
			
			msg(1);
		}
		include $tpl;
	}
	protected function dir_create($path) {
		if(is_dir($path)) return true;
		@mkdir($path,0777,true);
		return is_dir($path);
	}
	protected function dir_path($dirpath) {
		$dirpath = str_replace('\\', '/', $dirpath);
		if(substr($dirpath, -1) != '/') $dirpath = $dirpath.'/';
		return $dirpath;
	}
	protected function dir_copy($fromdir, $todir) {
		$fromdir = $this->dir_path($fromdir);
		$todir = $this->dir_path($todir);
		if(!is_dir($fromdir)) return false;
		if(!is_dir($todir)) $this->dir_create($todir);
		$list = glob($fromdir.'*');
		foreach($list as $v) {
			$path = $todir.basename($v);
			if(is_file($path) && !is_writable($path)) {
				@chmod($path, 0777);
			}
			if(is_dir($v)) {
				$this->dir_copy($v, $path);
			} else {
				@copy($v, $path);
				@chmod($path, 0777);
			}
		}
		return true;
		
	}
	
}

