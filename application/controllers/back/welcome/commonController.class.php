<?php
class commonController extends Controller{
	public function __construct(){
		parent::__construct();
		VG::$project_debug=false;
		
	}
	public function area(){
		extract($_POST);
		$area_title = isset($area_title) ? trim($area_title) : '';
		$area_extend = isset($area_extend) ? stripslashes($area_extend) : '';
		$areaid = isset($areaid) ? intval($areaid) : 0;
		$area_deep = isset($area_deep) ? intval($area_deep) : 0;
		$area_id= isset($area_id) ? intval($area_id) : 1;
		echo get_area_select($area_title, $areaid, $area_extend, $area_deep, $area_id);
	}
}