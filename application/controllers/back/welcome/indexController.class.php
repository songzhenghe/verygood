<?php
class indexController extends Controller{
	public function __construct(){
		parent::__construct();
		VG::$project_debug=false;
		$this->member_model=load_model('member');
		$this->login_back_model=load_model('login_back');
	}
    public function login($tpl){
	
		if(isset($_SESSION['MY'])){
            jump('back/frameset/index/index');//跳至后台首页
        }
	
		if($_POST){
			
			$username=trim($_POST['username']);
			$password=trim($_POST['password']);
			$_verycode=trim($_POST['_verycode']);
			if($username==''){
				msg(0,'账号不能为空！');
			}
			if($password==''){
				msg(0,'密码不能为空！');
			}
			if(strtolower($_verycode)!=strtolower($_SESSION['_verycode']) or $_verycode==''){
				unset($_SESSION['_verycode']);
				msg(0,'验证码错误！');
			}
			$username2=$this->db->escape($username);
			$data=$this->db->select("select * from __member__ where username='{$username2}' and groupid in(1,2,3) ");
			if(empty($data)){
				$this->login_back_model->record(array('username'=>$username,'password'=>md5($password),'message'=>'账号不存在'));
				msg(0,'账号不存在！');
			}
			if($this->member_model->encode_password($password)!=$data['password']){
				$this->login_back_model->record(array('username'=>$username,'password'=>md5($password),'message'=>'密码不正确'));
				msg(0,'密码不正确!');
			}
			
			if($data['groupid']==3){
				$this->login_back_model->record(array('username'=>$username,'password'=>md5($password),'message'=>'尝试登陆已禁用账号！'));
				msg(0,'对不起,您的账号已被管理员禁用!');
			}
			
			$big_priv=array();
			
			
			$_SESSION['MY']=array();
			$_SESSION['MY']['ISLOGIN']=1;
			$_SESSION['MY']['ID']=$data['userid'];
			$_SESSION['MY']['USER_NAME']=$data['username'];
			$_SESSION['MY']['GROUPID']=$data['groupid'];
			$_SESSION['MY']['PRIV']=$big_priv;

			

			$this->login_back_model->record(array('username'=>$username,'password'=>md5($password),'message'=>'登陆成功'));
			
			
			jump('back/frameset/index/index');//跳至后台首页
		}
		
		//允许登陆后台的ip
		$setting_1=load_setting(1);
		banip($setting_1['admin_ip_tag'],$setting_1['admin_ip']);
		
        include $tpl;
    }
	public function area(){
		extract($_POST);
		$area_title = isset($area_title) ? trim($area_title) : '';
		$area_extend = isset($area_extend) ? stripslashes($area_extend) : '';
		$areaid = isset($areaid) ? intval($areaid) : 0;
		$area_deep = isset($area_deep) ? intval($area_deep) : 0;
		$area_id= isset($area_id) ? intval($area_id) : 1;
		echo get_area_select($area_title, $areaid, $area_extend, $area_deep, $area_id);
	}
}
