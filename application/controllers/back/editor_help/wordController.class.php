<?php
class wordController extends accessController{
	public function __construct(){
		parent::__construct();
		
		
	}
	protected function get_file($dir, $ext = '', $fs = array()) {
		$files = glob($dir.'/*');
		if(!is_array($files)) return $fs;
		foreach($files as $file) {
			if(is_dir($file)) {
				if(is_file($file.'/index.php') && is_file($file.'/config.inc.php')) continue;
				$fs = $this->get_file($file, $ext, $fs);
			} else {
				if($ext) {
					if(preg_match("/\.($ext)$/i", $file)) $fs[] = $file;
				} else {
					$fs[] = $file;
				}
			}
		}
		return $fs;
	}
	protected function file_ext($filename) {
		return strtolower(trim(substr(strrchr($filename, '.'), 1)));
	}
	protected function dir_delete($dir) {
		if(substr($dir, -1) != '/') $dir = $dir.'/';
		if(!is_dir($dir)) return false;
		if(strpos($dir,'..')!==false) die("Cannot Remove System DIR $dir ");
		$list = glob($dir.'*');
		if($list) {
			foreach($list as $v) {
				is_dir($v) ? $this->dir_delete($v) : @unlink($v);
			}
		}
		return @rmdir($dir);
	}
	//处理word
	public function index($tpl){
		extract(VG::$gp_var);
		
		isset($action) or $action='';
		isset($submit) or $submit='';
		
		
		
		
		switch($action) {
			case 'upload':
				include BASEPATH.'upload.instance.php';
				if(!$_FILES['uploadfile']['size']){
					alert('请选择zip文件');
					exit;
				}
				
				$config=array(
					'field_name'=>'uploadfile',
					'upload_path'=>'./common/temp/',
					'allow_type'=>array('.zip'),//数组
					'max_size'=>'10',
					'unit'=>'M',
					'israndname'=>true,
					'iscreatedir'=>false,
					'iscreatebydate'=>false,
					'isoverride'=>true
				);
				
				$r=upload_instance($config);
				$name = date('YmdHis').mt_rand(10, 99).$this->userid;
				if($r['error_number']==0){
					$path=$r['file_abspath'].$r['new_name'];
					
					mkdir('./common/temp/'.$name);
					$zip = new Unzip;
					$zip->extract_zip($path, './common/temp/'.$name.'/');
					unlink($path);
					$F = $this->get_file('./common/temp/'.$name);
					if($F) {
						$htm = '';
						$max = 0;
						foreach($F as $f) {
							$ext = $this->file_ext($f);
							if(in_array($ext, array('htm', 'html'))) {
								$tmp = filesize($f);
								if($tmp > $max) $htm = str_replace('./common/temp/', '', $f);
								$max = $tmp;
							} else {
								in_array($ext, array('jpg', 'jpeg', 'gif', 'png')) or unlink($f);
							}
						}
						if($htm) {
							echo "<script>parent.Upsuccess('{$htm}');</script>";
							exit;
						} else {
							alert('系统未在压缩包内找到HTM文件');
							exit;
						}
					} else {
						alert('解压缩失败，请检查目录权限');
						exit;
					}
					
					
				}else{
					alert($r['error_message']);
					exit;
				}
				

			break;
			case 'read':
				VG::$project_debug=0;
				if($word && in_array($this->file_ext($word), array('htm', 'html'))) {
					$data = file_get_contents('./common/temp/'.$word);
					if($data) {
						if($charset) $data = mb_convert_encoding($data, 'UTF-8', $charset);
						if(preg_match("/<body[^>]*>([\s\S]+)<\/body>/i", $data, $m)) $data = trim($m[1]);
						$data = str_replace('<![if !vml]>', '', $data);
						$data = str_replace('<![endif]>', '', $data);
						$data = str_replace('<o:p>', '', $data);
						$data = str_replace('</o:p>', '', $data);
						$data = preg_replace("/ v:shapes=\"[^\"]*\">/i", ">", $data);
						if($wd_class) {
							$data = preg_replace("/[\s]?class=[\'|\"]?[^>]*[\'|\"]?/i", '', $data);
						}
						if($wd_style) {
							$data = preg_replace("/[\s]?style=[\'|\"]?[^>]*[\'|\"]?/i", '', $data);
						}
						if($wd_span) {
							$data = preg_replace("/<span[^>]*>/i", "", $data);
							$data = preg_replace("/<\/span>/i", "", $data);
						}
						if($wd_note) {
							$data = preg_replace("/<!--[\s\S]*-->/isU", '', $data);
						}
						if($wd_nr) {
							$data = str_replace('<p></p>', '', $data);
							$data = str_replace('<p> </p>', '', $data);
							$data = str_replace('<p>&nbsp;</p>', '', $data);
							$data = str_replace('<p>&nbsp;</p>', '', $data);
							$data = preg_replace('/($\s*$)|(^\s*^)/m', '', $data);
						}
						exit($data);
					}
				}
				exit('');
			break;
			case 'run':
				$code = stripslashes($code);
				$base = './common/temp/'.dirname($temp).'/';
				$code = str_replace('src="', 'src="'.$base, $code);
				echo '<html>';
				echo '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>';
				echo '<body>';
				echo $code;
				echo '</body>';
				echo '</html>';
			break;
			default:
				
				isset($moduleid) or msg(0,'lose moduleid');
				isset($uniq) or msg(0,'lose uniq');
				isset($itemid) or msg(0,'lose itemid');
				isset($controller) or msg(0,'lose controller');
				isset($tb) or msg(0,'lose tb');
				isset($editor) or msg(0,'lose editor');
				
				
				$this->moduleid=$moduleid;
				$this->uniq=$uniq;
				$this->itemid=$itemid;
				$this->control=$controller;
				$this->tb=$tb;
				$this->editor=$editor;
				if($submit) {
					
					$content = stripslashes($content);
					$dir = dirname($word);
					$base = VG::$config['base_url'].'common/temp/'.$dir.'/';
					$content = str_replace('src="', 'src="'.$base, $content);
					
					$uplog_model=load_model('uplog');

					$content=$uplog_model->save_remote(array('tb'=>$this->tb,'itemid'=>$this->itemid,'uniq'=>$this->uniq),$content, 'jpg|jpeg|gif|png', 1);
					
					$tmp = explode('/', $dir);
					$this->dir_delete('./common/temp/'.$tmp[0]);
				}
				include $tpl;
			break;
		}
	}
    
    
} 
