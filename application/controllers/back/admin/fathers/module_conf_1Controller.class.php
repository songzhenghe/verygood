<?php
//模块配置
class module_conf_1Controller extends accessController{
	protected $setting=array();
	protected static $menu;
	public $category=false;
    public function __construct(){
        parent::__construct();
		
		list($this->moduleid)=sscanf(__CLASS__,'module_conf_%dController');
		$this->module_en='admin';
		$this->module_cn='网站设置';
		$this->linkurl='?r=back/admin/index/index';
		
		$this->setting['pagesize']='100';
		
		defined('MD_ROOT') or define('MD_ROOT',APPPATH.'controllers/back/admin/');
		
    }
	public static function _menu(){
		self::$menu=array(
			array('修改密码','?r=back/admin/index/safe','main',1),
			array('网站设置','?r=back/admin/index/setting','main',1),
			array('更新数据表字段缓存','?r=back/admin/index/table_cache','main',1),
		);
		return self::$menu;
	}
	
}