<?php
//模块验证
class module_auth_1Controller extends module_conf_1Controller{
	protected static $auth_ctl;
	protected static $auth_act;
	protected $module_category;
    public function __construct(){
        parent::__construct();
		
		if(!$this->priv()) msg(0,'没有权限');
		$this->_init();
    }
	public static function _auth(){
		self::$auth_ctl=array(
			'index'=>array('name'=>'index','chinese'=>'网站设置','is_auth'=>1,'note'=>''),
			
		);
		self::$auth_act=array(
			'index'=>array(
				'safe'=>array('name'=>'safe','chinese'=>'修改密码','is_auth'=>1,'note'=>''),
				'setting'=>array('name'=>'setting','chinese'=>'网站设置','is_auth'=>1,'note'=>''),
				'table_cache'=>array('name'=>'table_cache','chinese'=>'更新数据表字段缓存','is_auth'=>1,'note'=>''),
			),
			
		);
		return array(self::$auth_ctl,self::$auth_act);
	}
	protected function _init(){
		
		
	}
	protected function priv(){
		
		$this->auth_is_valid(self::_auth());
		
		$flag=$this->open_for_everyone(self::_auth());
		if($flag==true) return true;
		
		$flag=parent::priv();
		if($flag==true) return true;
		
		
		$access_back_model=load_model('access_back');
		$data=$access_back_model->field('id')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid,'module'=>VG::$module,'controller'=>VG::$control,'action'=>VG::$action))->easy_find();
		if($data['id']>0) return true;
		
		
		return false;
		
	}
}