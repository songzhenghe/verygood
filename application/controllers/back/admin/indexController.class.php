<?php
class indexController extends module_auth_1Controller{
	public function __construct(){
		parent::__construct();
		$this->self_service=load_service('member');
		$this->self_model=load_model('member');
	}
    public function safe($tpl){
		
		if($_POST){
			if($this->self_model->check_blank2($_POST)){
				if($this->self_model->is_correct('old_password')){
					if($this->self_model->update_pwd()){
						msg(1);
					}else{
						msg(0);
					}
				}else{
					msg(0,'旧密码不正确！');
				}
			}else{
				msg(0,'旧密码、新密码不能为空！');
			}
		}
		
		include $tpl;
	}
	public function setting($tpl){
		if($_POST){
			$r=save_setting($this->moduleid,$_POST);
			msg($r);
		}
		
		$setting=load_setting($this->moduleid);
		extract($setting);
		include $tpl;
	}
	
	public function table_cache(){
		$dir=APPPATH.'runtime/table_cache/';
//		$files=scandir($dir);
//		foreach($files as $file){
//			if(in_array($file,array('.','..','index.html'))) continue;
//			unlink($dir.$file);
//		}
		$dh   =  opendir ( $dir );
		while ( false  !== ( $filename  =  readdir ( $dh ))) {
			 $files [] =  $filename ;
		}
		closedir($dh);
		foreach($files as $file){
			if(in_array($file,array('.','..','index.html'))) continue;
			unlink($dir.$file);
		}

		msg(1);
	}
	
} 
