<?php
//模块验证
class module_auth_9Controller extends module_conf_9Controller{
	protected static $auth_ctl;
	protected static $auth_act;
	protected $module_category;
    public function __construct(){
        parent::__construct();
		
		if(!$this->priv()) msg(0,'没有权限');
		$this->_init();
    }
	public static function _auth(){
		self::$auth_ctl=array(
			'index'=>array('name'=>'index','chinese'=>'后台登陆日志','is_auth'=>1,'note'=>''),
			'php_mylog'=>array('name'=>'php_mylog','chinese'=>'php错误日志','is_auth'=>1,'note'=>''),
			'mysql_mylog'=>array('name'=>'mysql_mylog','chinese'=>'mysql错误日志','is_auth'=>1,'note'=>''),
			
		);
		self::$auth_act=array(
			'index'=>array(
				'lst'=>array('name'=>'lst','chinese'=>'后台登陆日志','is_auth'=>1,'note'=>''),
				'lst2'=>array('name'=>'lst2','chinese'=>'后台操作日志','is_auth'=>1,'note'=>''),
				'clear2'=>array('name'=>'clear2','chinese'=>'清除后台操作日志','is_auth'=>1,'note'=>''),
				
			),
			'php_mylog'=>array(
				'clear'=>array('name'=>'clear','chinese'=>'清除日志','is_auth'=>1,'note'=>''),
				'index'=>array('name'=>'index','chinese'=>'日志内容','is_auth'=>1,'note'=>''),
			),
			'mysql_mylog'=>array(
				'clear'=>array('name'=>'clear','chinese'=>'清除日志','is_auth'=>1,'note'=>''),
				'index'=>array('name'=>'lst','chinese'=>'日志内容','is_auth'=>1,'note'=>''),
			),
			
		);
		return array(self::$auth_ctl,self::$auth_act);
	}
	protected function _init(){
		
		
	}
	protected function priv(){
		
		$this->auth_is_valid(self::_auth());
		
		$flag=$this->open_for_everyone(self::_auth());
		if($flag==true) return true;
		
		$flag=parent::priv();
		if($flag==true) return true;
		
		
		$access_back_model=load_model('access_back');
		$data=$access_back_model->field('id')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid,'module'=>VG::$module,'controller'=>VG::$control,'action'=>VG::$action))->easy_find();
		if($data['id']>0) return true;
		
		
		return false;
		
	}
}