<?php
//模块配置
class module_conf_9Controller extends accessController{
	protected $setting=array();
	protected static $menu;
	public $category=false;
    public function __construct(){
        parent::__construct();
		
		list($this->moduleid)=sscanf(__CLASS__,'module_conf_%dController');
		$this->module_en='log';
		$this->module_cn='日志管理';
		$this->linkurl='?r=back/log/index/index';
		
		$this->setting['pagesize']='100';
		
		defined('MD_ROOT') or define('MD_ROOT',APPPATH.'controllers/back/log/');
		
    }
	public static function _menu(){
		self::$menu=array(
			array('后台登陆日志','?r=back/log/index/lst','main',1),
			array('后台操作日志','?r=back/log/index/lst2','main',1),
			array('php日志','?r=back/log/php_mylog/index','main',1),
			array('清空php日志','?r=back/log/php_mylog/clear','main',1),
			
			array('mysql日志','?r=back/log/mysql_mylog/index','main',1),
			array('清空mysql日志','?r=back/log/mysql_mylog/clear','main',1),
			
		);
		return self::$menu;
	}
	
}