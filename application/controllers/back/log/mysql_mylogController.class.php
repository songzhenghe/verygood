<?php
class mysql_mylogController extends module_auth_9Controller{
	public function __construct(){
		parent::__construct();
		$this->file=APPPATH.'runtime/error_mysql_log.txt';
		VG::$project_debug=false;
	}
    public function index($tpl){
		$content=file_get_contents($this->file);
		echo $content;
	}
	public function clear($tpl){
		if($_POST){
			$r=file_put_contents($this->file,' ');
			msg($r);
		}
		include $tpl;
	}
} 
