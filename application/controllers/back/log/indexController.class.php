<?php
class indexController extends module_auth_9Controller{
	public function __construct(){
		parent::__construct();
		
	}
    public function lst($tpl){
		$gp=VG::get_post();
		extract($gp);
		$condition=" where 1=1 ";
		$extra=" order by logid desc ";
		
		isset($username) or $username='';
		if($username){
			$username2=$this->db->escape($username);
			$condition.=" and username like '%{$username2}%' ";
		}
		$self_service=load_service('login_back');
		$self_model=load_model('login_back');
		
		$pager=new Page($self_service->count($condition));
		$extra.=$pager->limit;
		$list=$self_service->lst($condition.$extra);
		include $tpl;
	}
    public function lst2($tpl){
		$gp=VG::get_post();
		extract($gp);
		$condition=" where 1=1 ";
		$extra=" order by logid desc ";
		
		isset($username) or $username='';
		if($username){
			$username2=$this->db->escape($username);
			$condition.=" and username like '%{$username2}%' ";
		}
		
		isset($qstring) or $qstring='';
		if($qstring){
			$qstring2=$this->db->escape($qstring);
			$condition.=" and qstring like '%{$qstring2}%' ";
		}
		
		isset($ip) or $ip='';
		if($ip){
			$ip2=$this->db->escape($ip);
			$condition.=" and ip = '{$ip2}' ";
		}
		
		isset($fromdate) or $fromdate='';
		if($fromdate){
			$tmp=strtotime($fromdate);
			$condition.=" and logtime >='{$tmp}' ";
		}
		
		isset($todate) or $todate='';
		if($todate){
			$tmp=strtotime($todate);
			$condition.=" and logtime <='{$tmp}' ";
		}
		
		$self_service=load_service('logop_back');
		$self_model=load_model('logop_back');
		
		$pager=new Page($self_service->count($condition));
		$extra.=$pager->limit;
		$list=$self_service->lst($condition.$extra);
		include $tpl;
	}
	public function clear2(){
		$self_model=load_model('logop_back');
		$self_model->clear();
		msg(1);
	}
	public function mod_intro($tpl){
		include $tpl;
	}
} 
