<?php
class indexController extends accessController{
	public function __construct(){
		parent::__construct();
		
		include BASEPATH.'upload.instance.php';
        
        $this->self_model=load_model('uplog');
	}
	//文件浏览器
	public function files_browser($tpl){
		$gp=VG::get_post();
		extract($gp);
		
		isset($moduleid) or $moduleid=1;
		
		isset($callback) or msg(0,'lose callback');
							
		$condition=" where 1=1 and userid='{$this->userid}' and moduleid='{$moduleid}' and itemid>0 ";
		$extra=" order by itemid desc ";
		
		isset($fileurl) or $fileurl='';
		if($fileurl){
			$fileurl2=$this->db->escape($fileurl);
			$condition.=" and fileurl like '%{$fileurl2}%' ";
		}
		
		isset($addtime_1) or $addtime_1='';
		if($addtime_1){
			$addtime_1_1=strtotime($addtime_1);
			$condition.=" and addtime >= '{$addtime_1_1}' ";
		}
		
		isset($addtime_2) or $addtime_2='';
		if($addtime_2){
			$addtime_2_1=strtotime($addtime_2);
			$condition.=" and addtime <= '{$addtime_2_1}' ";
		}
		
		$sql="select count(*) from __uplog__ {$condition}";
		$pager=new Page($this->db->get_field($sql));
		
		$extra.=$pager->limit;
		$sql="select id,fileurl,addtime from __uplog__ {$condition}{$extra}";
		$list=$this->db->mselect($sql);
			
		include $tpl;
	}
	
	
	public function up_img($tpl){
		extract(VG::get_post());
		$post=array();
		$post['callback']=$callback;
		$post['class']=$class;
		$post['i']=$i;
        
		$config=array(
			'field_name'=>'myfile',
			'upload_path'=>'./common/uploads/',
			'allow_type'=>array('.jpg','.png','.gif'),//数组
			'max_size'=>'10',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);

		/*
Array
(
    [origin_name] =>  Microsoft Excel 工作表.xls
    [new_name] => 14455784992159.xls
    [file_type] => .xls
    [file_size] => 6656
    [file_abspath] => ./common/uploads/2015/10/23/
    [file_relpath] => 2015/10/23/
    [error_number] => 0
    [error_message] => 文件上传成功！
)
		*/
	
		if($_POST){
			$r=upload_instance($config);
			if($r['error_number']==0){
				$path=$r['file_abspath'].$r['new_name'];
                list($itemid,$path)=$this->self_model->add(array('fileurl'=>$path));
                //$path=VG::$config['base_url'].trim($path,'./');
                echo "<script>parent.{$post['callback']}(['{$post['class']}','{$post['i']}','{$path}']);</script>";
                echo "<script>parent.$.colorbox.close();</script>";
			}else{
                msg(0,$r['error_message']);
			}
		}
        include $tpl;
	}
	public function up_file($tpl){
		extract(VG::get_post());
		$post=array();
		$post['callback']=$callback;
		$post['class']=$class;
		$post['i']=$i;
		$config=array(
			'field_name'=>'myfile',
			'upload_path'=>'./common/uploads/',
			'allow_type'=>array('.jpg','.png','.gif','.rar','.zip','.pdf','.txt','.doc','.docx','.ppt','.pptx','.xls','.xlsx','.swf'),//数组
			'max_size'=>'10',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);

		/*
Array
(
    [origin_name] =>  Microsoft Excel 工作表.xls
    [new_name] => 14455784992159.xls
    [file_type] => .xls
    [file_size] => 6656
    [file_abspath] => ./common/uploads/2015/10/23/
    [file_relpath] => 2015/10/23/
    [error_number] => 0
    [error_message] => 文件上传成功！
)
		*/
		if($_POST){
			
			$r=upload_instance($config);
			if($r['error_number']==0){
				$path=$r['file_abspath'].$r['new_name'];
                list($itemid,$path)=$this->self_model->add(array('fileurl'=>$path));
                //$path=VG::$config['base_url'].trim($path,'./');
                echo "<script>parent.{$post['callback']}(['{$post['class']}','{$post['i']}','{$path}']);</script>";
                echo "<script>parent.$.colorbox.close();</script>";
			}else{
                msg(0,$r['error_message']);
			}
		}
        include $tpl;
	}
    public function upload_0($tpl){
		VG::$project_debug=false;
		
		$gp=VG::get_post();
		extract($gp);
		
		isset($callback) or msg(0,'lose callback');
		
		$config=array(
			'field_name'=>'myfile',
			'upload_path'=>'./common/uploads/',
			'allow_type'=>array('.jpg','.png','.gif'),//数组
			'max_size'=>'10',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);

		/*
Array
(
    [origin_name] =>  Microsoft Excel 工作表.xls
    [new_name] => 14455784992159.xls
    [file_type] => .xls
    [file_size] => 6656
    [file_abspath] => ./common/uploads/2015/10/23/
    [file_relpath] => 2015/10/23/
    [error_number] => 0
    [error_message] => 文件上传成功！
)
		*/
		if($_POST){

		/*
Array
(
    [r] => back/upload/index/upload_0
    [callback] => aaa
    [moduleid] => 3
    [uniq] => b8aec7ba9fa2ace3f804f63b1ecd6073
    [itemid] => 0
    [controller] => index
)

		*/
		
			$r=upload_instance($config);
			if($r['error_number']==0){
				$path=$r['file_abspath'].$r['new_name'];
                list($itemid,$path)=$this->self_model->add(array('fileurl'=>$path));
                //$path=VG::$config['base_url'].trim($path,'./');
				echo "<script>window.parent.{$callback}(['{$path}']);window.parent.$.colorbox.close();</script>";
			}else{
				msg(0,$r['error_message']);
			}
		}
		
		include $tpl;
	}
    
	private function mkhtml($fn,$fileurl,$message)
	{
		$str='<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$fn.', \''.$fileurl.'\', \''.$message.'\');</script>';
		exit($str);
	}
    
	//编辑器上传图片
	public function ck_img()
	{
		VG::$project_debug=false;
		
		$gp=VG::get_post();
		extract($gp);
        
		if(empty($CKEditorFuncNum)) $this->mkhtml(1,'','错误的功能调用请求！');
		
		
		$config=array(
			'field_name'=>'upload',
			'upload_path'=>'./common/uploads/',
			'allow_type'=>array('.jpg','.png','.gif'),//数组
			'max_size'=>'10',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);

		/*
Array
(
    [origin_name] =>  Microsoft Excel 工作表.xls
    [new_name] => 14455784992159.xls
    [file_type] => .xls
    [file_size] => 6656
    [file_abspath] => ./common/uploads/2015/10/23/
    [file_relpath] => 2015/10/23/
    [error_number] => 0
    [error_message] => 文件上传成功！
)
		*/
		
			
        $r=upload_instance($config);
        if($r['error_number']==0){
            $path=$r['file_abspath'].$r['new_name'];
            list($itemid,$path)=$this->self_model->add(array('fileurl'=>$path));
            //$path=VG::$config['base_url'].trim($path,'./');
            $this->mkhtml($CKEditorFuncNum,$path,'上传成功！');
        }else{
            $this->mkhtml($CKEditorFuncNum,'',$r['error_message']);
        }
		
        
		
	}
	//编辑器上传附件
	public function ck_file()
	{
		VG::$project_debug=false;
		
		$gp=VG::get_post();
		extract($gp);
        
		if(empty($CKEditorFuncNum)) $this->mkhtml(1,'','错误的功能调用请求！');
		
		
		$config=array(
			'field_name'=>'upload',
			'upload_path'=>'./common/uploads/',
			'allow_type'=>array('.jpg','.png','.gif','.rar','.zip','.pdf','.txt','.doc','.docx','.ppt','.pptx','.xls','.xlsx','.swf'),//数组
			'max_size'=>'10',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);

		/*
Array
(
    [origin_name] =>  Microsoft Excel 工作表.xls
    [new_name] => 14455784992159.xls
    [file_type] => .xls
    [file_size] => 6656
    [file_abspath] => ./common/uploads/2015/10/23/
    [file_relpath] => 2015/10/23/
    [error_number] => 0
    [error_message] => 文件上传成功！
)
		*/

			
        $r=upload_instance($config);
        if($r['error_number']==0){
            $path=$r['file_abspath'].$r['new_name'];
            list($itemid,$path)=$this->self_model->add(array('fileurl'=>$path));
            //$path=VG::$config['base_url'].trim($path,'./');
            $this->mkhtml($CKEditorFuncNum,$path,'上传成功！');
        }else{
            $this->mkhtml($CKEditorFuncNum,'',$r['error_message']);
        }
		
	}
    
	//uploadify上传
	public function uploadify_files($tpl){
		VG::$project_debug=false;
		
		$gp=VG::get_post();
		extract($gp);
		
		$post=array();
		$post['callback']=$callback;
		$post['class']=$class;
		$post['i']=$i;
		$config=array(
			'field_name'=>'Filedata',
			'upload_path'=>'./common/uploads/',
			'allow_type'=>array('.jpg','.png','.gif','.rar','.zip','.pdf','.txt','.doc','.docx','.ppt','.pptx','.xls','.xlsx','.swf'),//数组
			'max_size'=>'10',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);

		/*
Array
(
    [origin_name] =>  Microsoft Excel 工作表.xls
    [new_name] => 14455784992159.xls
    [file_type] => .xls
    [file_size] => 6656
    [file_abspath] => ./common/uploads/2015/10/23/
    [file_relpath] => 2015/10/23/
    [error_number] => 0
    [error_message] => 文件上传成功！
)
		*/
		if($_POST){
			
			$r=upload_instance($config);
			if($r['error_number']==0){
				$path=$r['file_abspath'].$r['new_name'];
                list($itemid,$path)=$this->self_model->add(array('fileurl'=>$path));
                //$path=VG::$config['base_url'].trim($path,'./');
				echo json_encode(array('code'=>1,'msg'=>'success','url'=>$path,'callback'=>$post['callback'],'class'=>$post['class'],'i'=>$post['i']));
			}else{
				echo json_encode(array('code'=>0,'msg'=>$r['error_message'],'url'=>'','callback'=>$post['callback'],'class'=>$post['class'],'i'=>$post['i']));
			}
			
			exit;
		}
        include $tpl;

		
		
	}
    
    
} 
