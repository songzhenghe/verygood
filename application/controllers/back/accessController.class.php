<?php
//后台权限验证
class accessController extends core_conf{
    public function __construct(){
        parent::__construct();
		//跳至登陆页
        if(!isset($_SESSION['MY'])){
            jump('back/welcome/index/login');
        }
		
		$this->init();
		
    }
	protected function init(){
		$this->userid=$_SESSION['MY']['ID'];
		$this->username=$_SESSION['MY']['USER_NAME'];
		$this->groupid=$_SESSION['MY']['GROUPID'];
		$this->priv=$_SESSION['MY']['PRIV'];
		$this->_UNIQ=md5(time().rand(100000,999999));
		//$this->mylog=$this->loglog();
	}
	protected function priv(){
		if($this->groupid==1) return true;
		return false;
	}
	protected function loglog(){
		$model=load_model('logop_back');
		$model->record(array('kind'=>'','message'=>'','module'=>''));
		return $model;
	}
	protected function auth_is_valid($a){
		list($ctl,$act)=$a;
		if(!isset($ctl[VG::$control])) msg(0,VG::$control.' 权限规则不存在');
		if(!isset($act[VG::$control][VG::$action])) msg(0,VG::$control.':'.VG::$action.' 权限规则不存在');
	}
	protected function open_for_everyone($a){
		list($ctl,$act)=$a;
		if($ctl[VG::$control]['is_auth']==0) return true;
		if($act[VG::$control][VG::$action]['is_auth']==0) return true;
		return false;
	}
}