<?php
class website_accessController extends module_auth_2Controller{
	public function __construct(){
		parent::__construct();
		$this->self_service=load_service('member');
		$this->self_model=load_model('member');
	}
	public function panel($tpl){
		$userid=intval($_GET['userid']);
		if(!$userid) msg(0,'lose userid');
				
		if($_POST){
			$r=$this->self_service->panel($userid,$_POST);
			msg($r);
		}
		
		$conf=new core_conf();
		$module_list=$conf->module_list();
		
		list($checked)=$this->self_service->get_panel($userid);
		
		include $tpl;
	}
	public function a($tpl){
		$userid=intval($_GET['userid']);
		if(!$userid) msg(0,'lose userid');
		
		$moduleid=isset($_GET['moduleid'])?$_GET['moduleid']:1;
		$moduleid=abs(intval($moduleid));
		$moduleid=max(1,$moduleid);
		
		$conf=new core_conf();
		$module_list=$conf->module_list();
		
		if(!array_key_exists($moduleid,$module_list)){
			msg(0,'非法操作');
		}
		
		if($_POST){
			$r=$this->self_service->a($userid,$_POST);
			msg($r);
		}
		
		
		
		$mod=$conf->mod_single($moduleid);
		
		
		list($flag,$cat)=$conf->mod_category($moduleid);
		$flag=intval($flag);
		
		list($checked,$selected)=$this->self_service->get_a($userid,$moduleid);
		
		include $tpl;
	}
} 
