<?php
//模块验证
class module_auth_2Controller extends module_conf_2Controller{
	protected static $auth_ctl;
	protected static $auth_act;
	protected $module_category;
    public function __construct(){
        parent::__construct();
		
		if(!$this->priv()) msg(0,'没有权限');
		$this->_init();
    }
	public static function _auth(){
		self::$auth_ctl=array(
			'index'=>array('name'=>'index','chinese'=>'会员','is_auth'=>1,'note'=>''),
			'website_access'=>array('name'=>'website_access','chinese'=>'操作授权','is_auth'=>1,'note'=>''),
		);
		self::$auth_act=array(
			'index'=>array(
				'add'=>array('name'=>'add','chinese'=>'添加会员','is_auth'=>1,'note'=>''),
				'mod'=>array('name'=>'mod','chinese'=>'编辑会员','is_auth'=>1,'note'=>''),
				'lst'=>array('name'=>'lst','chinese'=>'会员列表','is_auth'=>1,'note'=>''),
				'del'=>array('name'=>'del','chinese'=>'删除会员','is_auth'=>1,'note'=>''),
				'setting'=>array('name'=>'setting','chinese'=>'模块设置','is_auth'=>1,'note'=>''),
			),
			'website_access'=>array(
				'panel'=>array('name'=>'panel','chinese'=>'操作面板','is_auth'=>1,'note'=>''),
				'a'=>array('name'=>'a','chinese'=>'后台权限','is_auth'=>1,'note'=>''),
			),
		);
		return array(self::$auth_ctl,self::$auth_act);
	}
	protected function _init(){
		
		
	}
	protected function priv(){
		
		$this->auth_is_valid(self::_auth());
		
		$flag=$this->open_for_everyone(self::_auth());
		if($flag==true) return true;
		
		$flag=parent::priv();
		if($flag==true) return true;
		
		
		$access_back_model=load_model('access_back');
		$data=$access_back_model->field('id')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid,'module'=>VG::$module,'controller'=>VG::$control,'action'=>VG::$action))->easy_find();
		if($data['id']>0) return true;
		
		
		return false;
		
	}
}