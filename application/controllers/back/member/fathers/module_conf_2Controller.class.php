<?php
//模块配置
class module_conf_2Controller extends accessController{
	protected $setting=array();
	protected static $menu;
	public $category=false;
    public function __construct(){
        parent::__construct();
		
		list($this->moduleid)=sscanf(__CLASS__,'module_conf_%dController');
		$this->module_en='member';
		$this->module_cn='会员';
		$this->linkurl='?r=back/member/index/index';
		
		$this->setting['pagesize']='100';
		
		defined('MD_ROOT') or define('MD_ROOT',APPPATH.'controllers/back/member/');
		
    }
	public static function _menu(){
		self::$menu=array(
			array('添加会员','?r=back/member/index/add','main',1),
			array('会员列表','?r=back/member/index/lst','main',1),
			array('模块设置','?r=back/member/index/setting','main',1),
			
		);
		return self::$menu;
	}
	
}