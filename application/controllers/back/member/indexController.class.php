<?php
class indexController extends module_auth_2Controller{
	public function __construct(){
		parent::__construct();
		$this->self_service=load_service('member');
		$this->self_model=load_model('member');
	}
    public function lst($tpl){
		$gp=VG::get_post();
		extract($gp);
		$condition=" where 1=1 ";
		$extra=" order by userid asc ";
		
		isset($userid) or $userid='';
		if($userid){
			$userid2=intval($userid);
			$condition.=" and userid = '{$userid2}' ";
		}
		isset($username) or $username='';
		if($username){
			$username2=$this->db->escape($username);
			$condition.=" and username like '%{$username2}%' ";
		}
		isset($groupid) or $groupid='';
		if($groupid){
			$groupid2=intval($groupid);
			$condition.=" and groupid = '{$groupid}' ";
		}
		
		$pager=new Page($this->self_service->count($condition));
		$extra.=$pager->limit;
		$list=$this->self_service->lst($condition.$extra);
		
		$groups=$this->db->mselect("select * from __member_group__ order by listorder asc");
		$groups_select=kv_builder($groups,'groupid','groupname',array(array(0,'--请选择--')));
		
		include $tpl;
	}
	public function add($tpl){
		if($_POST){
			$this->self_model->action='add';
			$r=$this->self_model->easy_insert($_POST);
			msg($r,$this->self_model->get_error());
		}
		$groups=$this->db->mselect("select * from __member_group__ order by listorder asc");
		$groups_radio=kv_builder($groups,'groupid','groupname');
		include $tpl;
	}
	public function mod($tpl){
		$userid=abs(intval($_GET['userid']));
		if(!$userid) msg(0);
		if($_POST){
			if($this->self_model->is_founder($userid) and $this->userid!=$userid){
				msg(0);
			}
			$password=trim($_POST['password']);
			if(!$password){
				unset($_POST['password']);
			}
			$this->self_model->action='mod';
			$this->self_model->userid=$userid;
			$r=$this->self_model->where(array('userid'=>$userid))->easy_update($_POST);
			msg($r,$this->self_model->get_error(),VG::$forward);
		}

		$data=$this->self_model->easy_find($userid);

		$groups=$this->db->mselect("select * from __member_group__ order by listorder asc");
		$groups_radio=kv_builder($groups,'groupid','groupname');
		include $tpl;
	}
	public function del(){
		msg(0,'功能被屏蔽');
		$userid=abs(intval($_GET['userid']));
		if(!$userid) msg(0);
		if($this->self_model->is_founder($userid)){
			msg(0);
		}
		$r=$this->self_model->easy_delete($userid);
		if($r){
			$this->db->query("delete from __access_back__ where userid='{$userid}'");
			$this->db->query("delete from __panel_back__ where userid='{$userid}'");
		}
		msg($r);
	}
	public function setting($tpl){
		if($_POST){
			$setting_file=APPPATH.'setting/'.$this->moduleid.'.php';
			$str="<?php\r\n";
			$str.='return ';
			$str.=var_export($_POST,true);
			$str.=';';
			$r=file_put_contents($setting_file, $str);
			msg($r);
		}
		
		$data=array();
		$data=array_merge($data,$this->setting);
		include $tpl;
	}
} 
