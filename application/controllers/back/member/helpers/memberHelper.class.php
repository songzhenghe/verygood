<?php
class memberHelper extends module_conf_2Controller {
	public function add(){
		echo 'add';
	}
	public function menu(){
		return module_conf_2Controller::_menu();
	}
	public function auth(){
		return module_auth_2Controller::_auth();
	}
	public function category(){
		$category_model=load_model('category_8');
		return $category_model->order('listorder asc')->easy_select();
	}
}