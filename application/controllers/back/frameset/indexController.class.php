<?php
class indexController extends accessController{
    public function ajax_0(){
		VG::$project_debug=false;
        echo time();
    }
    public function index($tpl){
        include $tpl;
    }
    public function left($tpl){
		VG::$project_debug=false;
		$member_service=load_service('member');
		list($checked_menu)=$member_service->get_panel($this->userid);
		
        //清理垃圾附件
        if(in_array(mt_rand(0,10000),range(20,30))){
            $uplog_model=load_model('uplog');
            $uplog_model->garbage();
        }
		
        include $tpl;
    }
    public function main($tpl){
		$test_service=load_service('test');
        include $tpl;
    }
    public function top($tpl){
		VG::$project_debug=false;
        include $tpl;
    }
	public function sys_info($tpl){
		$test_service=load_service('test');
		include $tpl;
	}
    public function logout(){
        unset($_SESSION['MY']);
        unset($_SESSION['MY2']);
        unset($_SESSION['UPLOADS']);
        $_SESSION=array();
        if(isset($_COOKIE[session_name()])){
            setcookie(session_name(),'',time()-3600,'/');
        }
        session_destroy();
        $u=url('back/welcome/index/login');
        echo "<script>window.parent.location.href='{$u}';</script>";
        exit;
    }
}