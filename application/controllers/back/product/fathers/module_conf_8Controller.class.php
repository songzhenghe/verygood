<?php
//模块配置
class module_conf_8Controller extends accessController{
	protected $setting=array();
	protected static $menu;
	public $category=true;
    public function __construct(){
        parent::__construct();
		
		list($this->moduleid)=sscanf(__CLASS__,'module_conf_%dController');
		$this->module_en='product';
		$this->module_cn='产品';
		$this->linkurl='?r=back/product/index/index';
		
		$setting_file=APPPATH.'setting/'.$this->moduleid.'.php';
		if(!file_exists($setting_file)){
			//msg(0,'配置文件不存在');
			copy(APPPATH.'setting/0.php',APPPATH.'setting/'.$this->moduleid.'.php');
		}
		$this->setting=(array) include($setting_file);
		
		defined('MD_ROOT') or define('MD_ROOT',APPPATH.'controllers/back/product/');
		
    }
	public static function _menu(){
		self::$menu=array(
			array('添加产品','?r=back/product/index/add','main',1),
			array('产品列表','?r=back/product/index/lst','main',1),
			array('模块设置','?r=back/product/index/setting','main',1),
			array('添加自定义字段','?r=back/fields/index/add&moduleid=8&tb='.VG::$db['dbprefix'].'product','main',1),
			array('自定义字段列表','?r=back/fields/index/lst&moduleid=8&tb='.VG::$db['dbprefix'].'product','main',1),
			array('添加分类','?r=back/product/category/add','main',1),
			array('分类列表','?r=back/product/category/lst','main',1),
			array('更新分类缓存','?r=back/product/category/cache','main',1),
		);
		return self::$menu;
	}
	
}