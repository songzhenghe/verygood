<?php
//模块验证
class module_auth_8Controller extends module_conf_8Controller{
	protected static $auth_ctl;
	protected static $auth_act;
	protected $module_category;
    public function __construct(){
        parent::__construct();
		
		if(!$this->priv()) msg(0,'没有权限');
		
		$this->_init();

    }
	public static function _auth(){
		self::$auth_ctl=array(
			'index'=>array('name'=>'index','chinese'=>'产品','is_auth'=>1,'note'=>''),
			'category'=>array('name'=>'category','chinese'=>'分类','is_auth'=>1,'note'=>''),
			'fields'=>array('name'=>'fields','chinese'=>'自定义字段','is_auth'=>1,'note'=>''),
		);
		self::$auth_act=array(
			'index'=>array(
				'add'=>array('name'=>'add','chinese'=>'添加产品','is_auth'=>1,'note'=>''),
				'mod'=>array('name'=>'mod','chinese'=>'编辑产品','is_auth'=>1,'note'=>''),
				'lst'=>array('name'=>'lst','chinese'=>'产品列表','is_auth'=>1,'note'=>''),
				'del'=>array('name'=>'del','chinese'=>'删除产品','is_auth'=>1,'note'=>''),
				'setting'=>array('name'=>'setting','chinese'=>'模块设置','is_auth'=>1,'note'=>''),
			),
			'category'=>array(
				'add'=>array('name'=>'add','chinese'=>'添加分类','is_auth'=>1,'note'=>''),
				'mod'=>array('name'=>'mod','chinese'=>'编辑分类','is_auth'=>1,'note'=>''),
				'lst'=>array('name'=>'lst','chinese'=>'分类列表','is_auth'=>1,'note'=>''),
				'del'=>array('name'=>'del','chinese'=>'删除分类','is_auth'=>1,'note'=>''),
				'cache'=>array('name'=>'cache','chinese'=>'更新分类缓存','is_auth'=>0,'note'=>''),
			),
			'fields'=>array(
				'add'=>array('name'=>'add','chinese'=>'添加自定义字段','is_auth'=>1,'note'=>''),
				'mod'=>array('name'=>'mod','chinese'=>'编辑自定义字段','is_auth'=>1,'note'=>''),
				'lst'=>array('name'=>'lst','chinese'=>'自定义字段列表','is_auth'=>1,'note'=>''),
				'del'=>array('name'=>'del','chinese'=>'删除自定义字段','is_auth'=>1,'note'=>''),
				'order'=>array('name'=>'order','chinese'=>'自定义字段排序','is_auth'=>1,'note'=>''),
			),
			
		);
		return array(self::$auth_ctl,self::$auth_act);
	}
	protected function _init(){
		
		if($this->groupid==1){
			$this->module_category='*';
		}else{
			$cat_back_model=load_model('cat_back');
			$category=$cat_back_model->field('id,catid')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid))->easy_find();
			$this->module_category=$category?explode(',',$category['catid']):array(0);
		}
	}
	protected function priv(){
		
		$this->auth_is_valid(self::_auth());
		
		$flag=$this->open_for_everyone(self::_auth());
		if($flag==true) return true;
		
		$flag=parent::priv();
		if($flag==true) return true;
		
		
		$access_back_model=load_model('access_back');
		$data=$access_back_model->field('id')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid,'module'=>VG::$module,'controller'=>VG::$control,'action'=>VG::$action))->easy_find();
		if($data['id']>0) return true;
		
		
		return false;
		
	}
	protected function check_catid($catid){
		if($this->module_category=='*') return true;
		if(!in_array($catid,$this->module_category)) msg(0,'没有权限');
		return true;
	}
	protected function check_itemid($itemid){
		$catid=0;//取得catid
		return $this->check_catid($catid);
	}
}