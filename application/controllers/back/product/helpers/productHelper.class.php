<?php
class productHelper extends module_conf_8Controller {
	public function add(){
		echo 'add';
	}
	public function menu(){
		return module_conf_8Controller::_menu();
	}
	public function auth(){
		return module_auth_8Controller::_auth();
	}
	protected function getCatTree($arr,$id=0,$lev=0){
	    $tree = array();
	    foreach($arr as $v) {
	        if($v['parentid'] == $id) {
	            $v['lev'] = $lev;
	            $tree[] = $v;
	            $tree = array_merge($tree,$this->getCatTree($arr,$v['catid'],$lev+1));
	        }
	    }
	    return $tree;
	}
	public function category(){
		$category_model=load_model('category_8');
		$data=$category_model->order('listorder asc')->easy_select();
		return $this->getCatTree($data);
	}
}