<?php
class categoryController extends module_auth_8Controller{
	public function __construct(){
		parent::__construct();
		$this->self_model=load_model('category_8');
	}
	public function index(){
		
	}
	public function add($tpl){
		if($_POST){
			$r=$this->self_model->add($_POST);
			msg($r,$this->self_model->get_error());
		}
		include $tpl;
	}
	public function mod($tpl){
		$catid=intval($_GET['catid']);
		if(!$catid) exit('lose catid');
		
		if($_POST){
			$r=$this->self_model->mod($catid,$_POST);
			msg($r,$this->self_model->get_error(),VG::$forward);
		}
		$data=$this->self_model->get_cat($catid);
		extract($data);
		include $tpl;
	}
	public function del($tpl){
		$catid=intval($_GET['catid']);
		if(!$catid) exit('lose catid');
		
		$r=$this->self_model->del($catid);
		msg($r);
	}
	public function lst($tpl){
		$all_cats=$this->self_model->all_cats();
		$category_arr=$this->self_model->getCatTree($all_cats);
		include $tpl;
	}
	public function cache($tpl){
		$r=$this->self_model->cache();
		msg($r);
	}
}