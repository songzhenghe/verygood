<?php
class indexController extends module_auth_8Controller{
	public function __construct(){
		parent::__construct();
		
		$this->self_service=load_service('product');
		$this->self_model=load_model('product');
		
		$this->fields_func_service=load_service('fields_func');
		$this->fields_func_service->moduleid=$this->moduleid;
		$this->fields_func_service->uniq=$this->_UNIQ;
		$this->fields_func_service->itemid=intval(VG::get_post('itemid'));
		$this->fields_func_service->controller=VG::$control;
		$this->fields_func_service->tb='product';
	}
	
    public function index(){
        // echo $this->moduleid;
		// p($this->SETTING);
		// p($this->setting);
		
		// $product_helper=load_module_helper('product');
		// $product_helper->add();
		
		// $menu=$product_helper->menu();
		// p($menu);
		
		// $auth=$product_helper->auth();
		// p($auth);
		
		// $cat=$product_helper->category();
		// p($cat);
		
		// echo MD_ROOT;
		// p($this->module_category);
		

    }
	public function add($tpl){
		
		$FD = $this->fields_func_service->fields_read();
		$this->fields_func_service->FD=$FD;
		if($_POST){
			$r=$this->self_model->add($_POST,$this->fields_func_service);
			msg($r);
		}
		
		$item=array();	
		
		include $tpl;
	}
	public function mod($tpl){
		$itemid=intval($_GET['itemid']);
		$itemid or msg(0,'lose itemid');
		
		$FD = $this->fields_func_service->fields_read();
		$this->fields_func_service->FD=$FD;
		if($_POST){
			$r=$this->self_model->mod($itemid,$_POST,$this->fields_func_service);
			msg($r);
		}
		
		$item = $this->self_model->easy_find($itemid);
		extract($item);
		
		include $tpl;
	}
	public function lst($tpl){
		
	}
	public function del(){
		
	}
	public function setting($tpl){
		if($_POST){
			$setting_file=APPPATH.'setting/'.$this->moduleid.'.php';
			$str="<?php\r\n";
			$str.='return ';
			$str.=var_export($_POST,true);
			$str.=';';
			$r=file_put_contents($setting_file, $str);
			msg($r);
		}
		
		$data=array();
		$data=array_merge($data,$this->setting);
		include $tpl;
	}
} 
