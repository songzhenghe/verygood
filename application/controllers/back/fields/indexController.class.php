<?php
class indexController extends accessController{
	public function __construct(){
		parent::__construct();
		
		$tb=isset($_GET['tb'])?trim($_GET['tb']):'';
		if(!$tb) msg(0,'lose tb');
		$len = strlen(VG::$db['dbprefix']);
		if(substr($tb, 0, $len) == VG::$db['dbprefix']) $tb = substr($tb, $len);
		$this->tb=$tb;
		$this->moduleid=intval($_GET['moduleid']);
		$this->moduleid or msg(0,'lose moduleid');
		
		//权限
		if(!$this->priv()) msg(0,'没有权限');
		//
		
		$this->forward=url('back/fields/index/lst',"moduleid={$this->moduleid}&tb={$this->tb}");
		
		
		$this->self_service=load_service('fields');
		$this->self_model=load_model('fields');
		
		$this->self_service->tb=$tb;
		
	}
	protected function priv(){
		
		$conf=new core_conf();
		$auth=$conf->mod_auth($this->moduleid);
		
		
		$this->auth_is_valid($auth);
		
		$flag=$this->open_for_everyone($auth);
		if($flag==true) return true;
		
		$flag=parent::priv();
		if($flag==true) return true;
		
		
		$access_back_model=load_model('access_back');
		
		$module=$this->MODULE[$this->moduleid]['module_en'];
		
		
		$data=$access_back_model->field('id')->where(array('userid'=>$this->userid,'moduleid'=>$this->moduleid,'module'=>$module,'controller'=>VG::$control,'action'=>VG::$action))->easy_find();
		if($data['id']>0) return true;
		
		
		return false;
		
	}
    public function index(){
		
	}
	public function add($tpl){
		if($_POST){
			if($this->self_service->pass($_POST)){
				$r=$this->self_service->add($_POST);
				msg($r,'', $this->forward);
			}else{
				msg(0,$this->self_service->errmsg);
			}
		}
		include $tpl;
	}
	public function mod($tpl){
		$_GET['itemid'] or msg();
		$this->self_service->itemid = intval($_GET['itemid']);
		if($_POST){
			if($this->self_service->pass($_POST)) {
				$r=$this->self_service->edit($_POST);
				msg($r,'', $this->forward);
			} else {
				msg($this->self_service->errmsg);
			}
		}
		extract($this->self_service->get_one());
		include $tpl;
	}
	public function lst($tpl){
		list($items,$pages,$lists) = $this->self_service->get_list(array('moduleid'=>$this->moduleid,'tb'=>$this->tb));
		$this->self_service->cache_fields($this->moduleid);
		include $tpl;
	}
	public function del($tpl){
        msg(0,'删除功能被屏蔽');
		$_GET['itemid'] or msg();
		$itemid=intval($_GET['itemid']);
		$r=$this->self_service->delete($itemid);
		msg($r,'', $this->forward);
	}
	public function order($tpl){
		$r=$this->self_service->order($_POST['listorder']);
		msg($r,'', $this->forward);
	}
} 
