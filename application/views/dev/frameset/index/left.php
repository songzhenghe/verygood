<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>left.php</title>
<?php load_view('dev/cssjs');?>
<script src="<?php echo VG::$config['base_url'];?>common/js/jquery.tree.js"></script>

<script>
$(document).ready(function(e) {
	$('#files').tree();
});
</script>

<style>
body{
	background:#fafafa url(./application/static/dev/images/left_bg.png) repeat center center;	
}
</style>

</head>
<body style="padding-left:10px;">


<h4>操作面板</h4>
<ul id="files">
	<li><a href="javascript:void(0);">欢迎使用</a>
		<ul>
			<li><a href="<?php echo url('dev/frameset/index/main');?>" target="main">欢迎使用</a></li>
			<li><a href="<?php echo url('dev/frameset/index/sys_info');?>" target="main">系统信息</a></li>
			<li><a href="<?php echo url('dev/frameset/index/phpinfo');?>" target="main">phpinfo</a></li>
        </ul>
    </li>
	<li><a href="javascript:void(0);">缓存</a>
		<ul>
			<li><a href="<?php echo url('dev/cache_part/index/table_cache');?>" target="main">更新数据表字段缓存</a></li>
			
        </ul>
    </li>
	<li><a href="javascript:void(0);">模块</a>
		<ul>
			<li><a href="<?php echo url('dev/module/index/create_module');?>" target="main">新建模块</a></li>
			
        </ul>
    </li>
	
	
</ul>      

<script>
$(document).ready(function(){ 
	//每十分钟请求一次防止退出
	var url='<?php echo url('dev/frameset/index/ajax_0');?>';
	setInterval(function(){
		$.get(url,{},function(data){
			//alert(data);	
		},'html');
	},600000);
});
</script>
</body>
</html>