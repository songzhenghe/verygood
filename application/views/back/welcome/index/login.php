<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登陆</title>
<link rel="stylesheet" href="<?php echo static_url('css/');?>login.css" type="text/css">
</head>
<body>


<h3 class="title">登陆</h3>
<div id="login-box">
<div class="tip">请输入账号和密码</div>
<form action="" method="post">
<p class="center_row">账　号：<input type="text" name="username" id="" class="input" autocomplete="off"></p>
<p class="center_row">密　码：<input type="password" name="password" id="" class="input" autocomplete="off"></p>
<p class="center_row">验证码：<input type="text" name="_verycode" id="_verycode" class="input" style="width:205px;" autocomplete="off"> <span style="cursor:pointer;"><img src="?r=back/index/index/captcha&ts=<?php echo time();?>" title="点击刷新" alt="验证码" onclick="this.src='?r=back/index/index/captcha&ts='+Math.random();"></span></p>
<p class="right_row"><input type="submit" value="登陆" class="bluebutton"></p>
</form>
</div>
</body>
</html>