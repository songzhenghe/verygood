<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
</head>
<body>
<?php //$this->load->view('header');?>

<br />
<br />
<br />
<br />

<table width="90%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td colspan="2" align="center">系统信息！</td>
    </tr>
  <tr>
    <td align="right">时区：</td>
    <td align="left"><?php echo date_default_timezone_get()?> <?php echo date('Y-m-d H:i:s');?></td>
  </tr>
  <tr>
    <td align="right">上传文件大小：</td>
    <td align="left"><?php echo ini_get('upload_max_filesize')?></td>
  </tr>
  <tr>
    <td align="right">主机：</td>
    <td align="left"><?php echo $_SERVER['SERVER_NAME'];?></td>
  </tr>
  <tr>
    <td align="right">服务器：</td>
    <td align="left"><?php echo $_SERVER['SERVER_SOFTWARE'];?></td>
  </tr>
  <tr>
    <td align="right">php 版本：</td>
    <td align="left"><?php echo phpversion();?></td>
  </tr>
  <tr>
    <td align="right">魔术转义：</td>
    <td align="left"><?php echo get_magic_quotes_gpc()?'开启':'关闭';?></td>
  </tr>
  <tr>
    <td align="right">数据库版本：</td>
    <td align="left"><?php echo $test_service->dbVersion();?></td>
  </tr>
  <tr>
    <td align="right">网站首页：</td>
    <td align="left"><a href="<?php echo url('front/index/index/index');?>" target="_blank">访问</a></td>
  </tr>
</table>


<?php //$this->load->view('footer');?>
</body>
</html>