<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>left.php</title>
<?php load_view('back/cssjs');?>
<script src="<?php echo VG::$config['base_url'];?>common/js/jquery.tree.js"></script>

<script>
$(document).ready(function(e) {
	$('#files').tree();
});
</script>

<style>
body{
	background:#fafafa url(./application/static/back/images/left_bg.png) repeat center center;	
}
</style>

</head>
<body style="padding-left:10px;">

<?php
$conf=new core_conf();
$module_list=$conf->module_list();
?>
<h4>操作面板</h4>
<ul id="files">
	<li><a href="javascript:void(0);">欢迎使用</a>
		<ul>
			<li><a href="<?php echo url('back/frameset/index/main');?>" target="main">欢迎使用</a></li>
			<li><a href="<?php echo url('back/frameset/index/sys_info');?>" target="main">系统信息</a></li>
        </ul>
    </li>
	<?php
	foreach($module_list as $k=>$v){
		$menu=$conf->mod_menu($k);
	?>
	<li><a href="javascript:void(0);"><?php echo $v['module_cn'];?></a>
		<ul>
			<?php
			foreach($menu as $kk=>$vv){
				isset($checked_menu[$k]) or $checked_menu[$k]=array();
				if($vv[3] && $this->groupid==2 && !in_array($vv[1],$checked_menu[$k])) continue;
			?>
			<li><a href="<?php echo $vv[1];?>" target="<?php echo $vv[2];?>"><?php echo $vv[0];?></a></li>
			<?php
			}
			?>
        </ul>
    </li>
	<?php
	}
	?>
	
	
</ul>      

<script>
$(document).ready(function(){ 
	//每十分钟请求一次防止退出
	var url='<?php echo url('back/frameset/index/ajax_0');?>';
	setInterval(function(){
		$.get(url,{},function(data){
			//alert(data);	
		},'html');
	},600000);
});
</script>
</body>
</html>