<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<?php 
load_view('back/cssjs');
?>
<script>

</script>
</head>
<body >

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">管理字段 [<?php echo $this->tb;?>]</div>
</div>
<div class="bottom">

<form method="post">

<table cellpadding="2" cellspacing="1" class="mytable" width="99%">
<tr>
<td align="center"><input type="checkbox" onclick="checkall(this.form);"/></td>
<td align="center">排序</td>
<td align="center">字段</td>
<td align="center">字段名称</td>
<td align="center">字段属性</td>
<td align="center">表单类型</td>
<td align="center">表单name</td>
<td align="center">表单调用</td>
<td align="center">内容调用</td>
<td align="center">显示</td>
<td align="center">前台</td>
<td align="center">操作</td>
</tr>
<?php foreach($lists as $k=>$v) {?>
<tr align="center">
<td><input type="checkbox" name="itemid[]" value="<?php echo $v['itemid'];?>"/></td>
<td><input name="listorder[<?php echo $v['itemid'];?>]" type="text" size="2" value="<?php echo $v['listorder'];?>"/></td>
<td><?php echo $v['name'];?></td>
<td><?php echo $v['title'];?></td>
<td><?php echo $v['type'];?><?php echo $v['length'] ? '('.$v['length'].')' : '';?></td>
<td><?php echo $v['html'];?></td>
<td>post_fields[<?php echo $v['name'];?>]</td>
<td>{fields_show(<?php echo $v['itemid'];?>)}</td>
<td>{fields_k_v(<?php echo $v['itemid'];?>,array)}</td>
<td><?php echo $v['display'] ? '是' : '否';?></td>
<td><?php echo $v['front'] ? '是' : '否';?></td>
<td>
<a href="<?php echo url('back/fields/index/mod',"&moduleid={$this->moduleid}&tb={$this->tb}&itemid={$v['itemid']}");?>"><img src="<?php echo static_url('images/mod.png');?>" alt="编辑"></a>&nbsp;
<a href="<?php echo url('back/fields/index/del',"&moduleid={$this->moduleid}&tb={$this->tb}&itemid={$v['itemid']}");?>" onclick="return confirm('确定要删除么？');"><img src="<?php echo static_url('images/del.png');?>" alt="删除"></a>
</td>
</tr>
<?php }?>
</table>
<br />

<div class="">
&nbsp;&nbsp;
<input type="submit" value="更新排序" class="button" onclick="this.form.action='<?php echo smart_url(array('action'=>'order','get'=>"moduleid={$this->moduleid}&tb={$this->tb}"));?>';"/>
</div>
</form>
<div class="" style="text-align:center"><?php echo $pages;?></div>
<br/>

</div>
</div>

<script>
function checkall(f, t) {
	var t = t ? t : 1;
	for(var i = 0; i < f.elements.length; i++) {
		var e = f.elements[i];
		if(e.type != 'checkbox') continue;
		if(t == 1) e.checked = e.checked ? false : true;
		if(t == 2) e.checked = true;
		if(t == 3) e.checked = false;	
	}
}
</script>

</body>
</html>