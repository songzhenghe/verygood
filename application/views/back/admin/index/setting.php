<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">网站设置</div>
</div>
<div class="bottom">

<form action="" method="post">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="right">后台限制IP开关：</td>
    <td align="left">
    <?php echo form_radio('admin_ip_tag',array(0=>'关闭',1=>'开启'),$admin_ip_tag);?>
    </td>
  </tr>
  <tr>
    <td align="right">允许登陆后台的IP：</td>
    <td align="left">
    <textarea name="admin_ip" id="" cols="60" rows="5"><?php echo $admin_ip;?></textarea>
    <br />
请谨慎设置，以免发生不可登录后台的情况<br/>IP段用*代替数字，例如192.168.*.*<br/>多个IP地址或者IP段用|分隔，例如192.168.1.2|10.0.*.*
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>

</div>
</div>


<?php //$this->load->view('footer');?>
</body>
</html>