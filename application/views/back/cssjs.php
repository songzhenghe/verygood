


<link rel="stylesheet" type="text/css" href="<?php echo VG::$config['base_url'];?>common/css/colorbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo static_url('css/');?>content.css">
<link rel="stylesheet" type="text/css" href="<?php echo VG::$config['base_url'];?>common/css/tree.css">
<!--<link rel="stylesheet" type="text/css" href="<?php echo VG::$config['base_url'];?>common/css/selectable/style.css">-->


<script src="<?php echo VG::$config['base_url'];?>common/js/jquery.js"></script>
<script src="<?php echo VG::$config['base_url'];?>common/js/common.js"></script>
<script src="<?php echo VG::$config['base_url'];?>common/js/jquery.colorbox.js"></script>
<!--<script src="<?php echo VG::$config['base_url'];?>common/js/jQselectable.js"></script>-->
<script src="<?php echo static_url('js/');?>addon_fields.js"></script>
<script src="<?php echo static_url('js/');?>table.js"></script>

<script type="text/javascript" src="<?php echo VG::$config['base_url'];?>common/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript">
	window.CKEDITOR_BASEPATH='<?php echo VG::$config['base_url'];?>common/ckeditor/';
    CKEDITOR.config.filebrowserImageUploadUrl= '<?php echo url('back/upload/index/ck_img');?>';
	CKEDITOR.config.filebrowserLinkUploadUrl='<?php echo url('back/upload/index/ck_file');?>';
</script>

<script src="<?php echo VG::$config['base_url'];?>common/js/laydate.dev.js"></script>

<script>
$(document).ready(function(e) {
    $(".bit_file_up").colorbox({width:"500px", height:"400px", iframe:true, opacity:0.3});
    $(".files_browser").colorbox({width:"800px", height:"600px", iframe:true, opacity:0.3});
    $(".editor_help").colorbox({width:"800px", height:"600px", iframe:true, opacity:0.3});
});
</script>
