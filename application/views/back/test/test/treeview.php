<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>


<script src="<?php echo VG::$config['base_url'];?>common/js/jquery.cookie.js"></script>
<script src="<?php echo VG::$config['base_url'];?>common/js/jquery.treeview.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo VG::$config['base_url'];?>common/css/jquery.treeview.css">



<script type="text/javascript">
	$(document).ready(function(e) {
		$("#category").treeview({
			persist: "cookie",
			collapsed: true,
			unique: false
		});
    });
</script>

</head>
<body >
<?php //$this->load->view('header');?>
<script>

</script>
<br />
<br />
<br />
<br />

	<ul id="category">
		
        <?php
		$model=load_model('category_8');
        list($flag,$html)=$model->deep_tree_ul('?a=1',0);
		echo $html;
		?>
		
	</ul>


<?php //$this->load->view('footer');?>
</body>
</html>