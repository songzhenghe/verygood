<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>

</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">添加信息专案</div>
</div>
<div class="bottom">
<p style="color:red;text-align:center;font-weight:bold;"><?php echo isset($ERROR_MSG)?$ERROR_MSG:'';?></p>
<form action="" method="post" id="myform" onsubmit="return check();">
<input type="hidden" name="addtime" value="<?php echo time();?>">
<input type="hidden" name="username" value="<?php echo $this->username;?>">
<input type="hidden" name="customer_id" value="<?php echo $customer_id;?>">
<input type="hidden" name="_UNIQ" value="<?php echo $this->_UNIQ;?>">

<input type="hidden" name="edittime" value="<?php echo time();?>">
<input type="hidden" name="editor" value="<?php echo $this->username;?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="right">&nbsp;</td>
    <td align="left"><?php echo customer_name($customer_id);?></td>
  </tr>
  <tr>
    <td align="right">分类：</td>
    <td align="left"><?php echo $this->category_model->my_category_select('catid',$catid,'请选择','',$customer_id);?></td>

  </tr>
  <tr>
    <td align="right"><span class="red">标题：</span></td>
    <td align="left"><input type="text" name="title" id="title" class="input" value="<?php echo $title;?>" /><span id="dtitle" class="f_red"></span></td>
  </tr>
  <tr>
    <td align="right">英文标题：</td>
    <td align="left"><input type="text" name="entitle" id="entitle" class="input" value="<?php echo $entitle;?>" /><span id="dentitle" class="f_red"></span></td>
  </tr>
  <tr>
    <td align="right">提问日期：</td>
    <td align="left"><?php echo dcalendar('ztime',$ztime);?></td>
  </tr>
  <tr>
    <td align="right">提问者：</td>
    <td align="left"><input type="text" name="askor" id="askor" class="input" value="<?php echo $askor;?>" /></td>
  </tr>
  <tr>
    <td align="right"><span class="red">信息专案需求：</span></td>
    <td align="left">
      <textarea name="xuqiu" id="xuqiu" cols="50" rows="5"><?php echo $xuqiu;?></textarea>
      <br />
[<a href="<?php echo url("back/upload/index/uploadify_files","callback=callback_bit_file_up_0")?>&moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?>&tb=zhuanan" class="bit_file_up">多文件上传</a>]&nbsp;[<a href="<?php echo url("back/upload/index/files_browser","callback=callback_files_browser_0")?>&moduleid=<?php echo $this->moduleid;?>" class="files_browser">文件库</a>]
<ul id="target_bit_file_up_0"></ul>
<script>
function callback_bit_file_up_0(arr){
	//[path]
	var ext=arr[0].substring(arr[0].lastIndexOf("."),arr[0].length).toLowerCase();//后缀名
	if(ext=='.jpg' || ext=='.png' || ext=='.gif'){
		var thumb="<img src=\""+arr[0]+"\" width=\"30\">";	
	}else{
		var thumb="<img src=\"/common/images/att.png\" width=\"30\">";	
	}
	var html1="[<a href=\"javascript:void(0);\" onclick=\"_insert_pic('xuqiu','"+arr[0]+"');\">以图片插入</a>] ";
	var html2="[<a href=\"javascript:void(0);\" onclick=\"_insert_att('xuqiu','"+arr[0]+"');\">以附件插入</a>] ";
	$("#target_bit_file_up_0").append("<li>"+thumb+" "+html1+html2+"</li>");
	return true;
}
function callback_files_browser_0(arr){
	if(parseInt(arr[1])==1){
		_insert_pic('xuqiu',arr[0]);
	}else{
		_insert_att('xuqiu',arr[0]);
	}
	return true;
}
</script>
    </td>
  </tr>
  <tr>
    <td align="right"><span class="red">解决方案：</span></td>
    <td align="left">
      <textarea name="content" id="content" cols="50" rows="5"><?php echo $content;?></textarea>
      <br />
[<a href="<?php echo url("back/upload/index/uploadify_files","callback=callback_bit_file_up_1")?>&moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?>&tb=zhuanan" class="bit_file_up">多文件上传</a>]&nbsp;[<a href="<?php echo url("back/upload/index/files_browser","callback=callback_files_browser_1")?>&moduleid=<?php echo $this->moduleid;?>" class="files_browser">文件库</a>]
<ul id="target_bit_file_up_1"></ul>
<script>
function callback_bit_file_up_1(arr){
	//[path]
	var ext=arr[0].substring(arr[0].lastIndexOf("."),arr[0].length).toLowerCase();//后缀名
	if(ext=='.jpg' || ext=='.png' || ext=='.gif'){
		var thumb="<img src=\""+arr[0]+"\" width=\"30\">";	
	}else{
		var thumb="<img src=\"/common/images/att.png\" width=\"30\">";	
	}
	var html1="[<a href=\"javascript:void(0);\" onclick=\"_insert_pic('content','"+arr[0]+"');\">以图片插入</a>] ";
	var html2="[<a href=\"javascript:void(0);\" onclick=\"_insert_att('content','"+arr[0]+"');\">以附件插入</a>] ";
	$("#target_bit_file_up_1").append("<li>"+thumb+" "+html1+html2+"</li>");
	return true;
}
function callback_files_browser_1(arr){
	if(parseInt(arr[1])==1){
		_insert_pic('content',arr[0]);
	}else{
		_insert_att('content',arr[0]);
	}
	return true;
}
</script>
    </td>
  </tr>
  <tr>
    <td align="right">发布日期：</td>
    <td align="left"><?php echo dcalendar('fbtime',$fbtime);?></td>
  </tr>
  <tr>
    <td align="right">状态：</td>
    <td align="left"><?php echo form_radio('hidden',array(1=>'显示',2=>'隐藏'),$hidden);?></td>
  </tr>
  
  <?php echo $FD ? $this->fields_func_service->fields_html('<td align="right">', '<td>', $item) : '';?>
  <tr>
    <td colspan="2" align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>

</div>
</div>

<script>
$(document).ready(function(e) {
    $('#myform').get(0).reset();
});

function check() {
	var l;
	var f;

	f = 'title';
	l = Dd(f).value.length;
	if(l < 2) {
		Dmsg('标题最少2字，当前已输入'+l+'字', f);
		return false;
	}
	
	<?php echo $FD ? $this->fields_func_service->fields_js() : '';?>
	return true;
}
</script>

<script type="text/javascript">
    CKEDITOR.config.filebrowserImageUploadUrl+= '&moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?>&tb=zhuanan';
	CKEDITOR.config.filebrowserLinkUploadUrl+='&moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?>&tb=zhuanan';
</script>


<script type="text/javascript">
	CKEDITOR.replace('xuqiu', {"width":600,"height":200});
	CKEDITOR.replace('content', {"width":600,"height":200});
</script>



<?php //$this->load->view('footer');?>
</body>
</html>