<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<?php 
load_view('back/cssjs');
?>
</head>
<body >
<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">后台登陆日志</div>
</div>
<div class="bottom">

<form action="" method="get">
<input type="hidden" name="r" value="<?php echo $_GET['r'];?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">用户名：<input type="text" name="username" id="" value="<?php echo $username;?>"> <input type="submit" value="搜索" name="submit" class="button"> <input type="button" value="重置" onclick="location.href='<?php echo smart_url();?>';" class="button"></td>
  </tr>
</table>
</form>
<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="center">编号</td>
    <td align="center">用户名</td>
    <td align="center">密码</td>
    <td align="center">ip</td>
    <td align="center">时间</td>
    <td align="center">消息</td>
    <td align="center">浏览器</td>
  </tr>
  <?php
  foreach($list as $k=>$v){
  ?>
  <tr>
    <td align="center"><?php echo $v['logid'];?></td>
    <td align="center"><?php echo $v['username'];?></td>
    <td align="center"><?php echo $v['password'];?></td>
    <td align="center"><?php echo $v['loginip'];?></td>
    <td align="center"><?php echo date('Y-m-d H:i:s',$v['logintime']);?></td>
    <td align="center"><?php echo $v['message'];?></td>
    <td align="center"><input type="text" name="" id="" value="<?php echo $v['agent'];?>"></td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="7" align="center"><?php echo $pager->fpage();?></td>
  </tr>
</table>

</div>
</div>

</body>
</html>