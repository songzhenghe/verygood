<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理中心</title>
<?php 
load_view('back/cssjs');
?>
</head>
<body>
<?php load_view('back/log/header');?>
<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">后台操作日志</div>
</div>
<div class="bottom">

<form action="" method="get">
<input type="hidden" name="r" value="<?php echo $_GET['r'];?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
	用户名: <input type="text" name="username" value="<?php echo $username;?>" id="" class="input">&nbsp;
    地址: <input type="text" name="qstring" value="<?php echo $qstring;?>" id="" class="input">&nbsp;<br />
    IP: <input type="text" name="ip" value="<?php echo $ip;?>" id="" class="input">&nbsp;
    <?php echo dcalendar('fromdate', $fromdate);?>至
    <?php echo dcalendar('todate', $todate);?>

     <input type="submit" value="搜索" name="submit" class="button"> <input type="button" value="重置" onclick="location.href='<?php echo smart_url();?>';" class="button"></td>
  </tr>
</table>
</form>
<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td colspan="6" align="center"><a onclick="return confirm('确定要清除么？');" href="<?php echo url('back/log/index/clear2');?>">清除90天前日志</a></td>
    </tr>
  <tr>
    <td align="center">ID</td>
    <td align="center">用户名</td>
    <td align="center">地址</td>
    <td align="center">IP</td>
    <td align="center">客户端</td>
    <td align="center">时间</td>
    </tr>
  <?php
  foreach($list as $k=>$v){
  ?>
  <tr>
    <td align="center"><?php echo $v['logid'];?></td>
    <td align="center"><?php echo $v['username'];?></td>
    <td align="center"><input type="text" name="input" id="input" value="<?php echo $v['qstring'];?>" /></td>
    <td align="center"><?php echo $v['ip'];?></td>
    <td align="center"><input type="text" name="input" id="input" value="<?php echo $v['agent'];?>" /></td>
    <td align="center"><?php echo date('Y-m-d H:i:s',$v['logtime']);?></td>
    </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="6" align="center"><?php echo $pager->fpage();?></td>
  </tr>
</table>

</div>
</div>

<?php load_view('back/log/footer');?>
</body>
</html>