<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">编辑产品</div>
</div>
<div class="bottom">

<form action="" method="post" id="myform" onsubmit="return check();">
<input type="hidden" name="_UNIQ" value="<?php echo $this->_UNIQ;?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="right">分类：</td>
    <td align="left"><input type="text" name="catid" id="" class="input" value="<?php echo $catid;?>" /></td>

  </tr>
  <tr>
    <td align="right">标题：</td>
    <td align="left"><input type="text" name="title" id="title" class="input" value="<?php echo $title;?>" /><span id="dtitle" class="f_red"></span></td>
  </tr>
  <tr>
    <td align="right">内容：</td>
    <td align="left"><input type="text" name="content" id="content" class="input" value="<?php echo $content;?>" /></td>
  </tr>
  <?php echo $FD ? $this->fields_func_service->fields_html('<td align="right">', '<td>', $item) : '';?>
  <tr>
    <td colspan="2" align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>

</div>
</div>

<script>
$(document).ready(function(e) {
    $('#myform').get(0).reset();
});

function check() {
	var l;
	var f;

	f = 'title';
	l = Dd(f).value.length;
	if(l < 2) {
		Dmsg('标题最少2字，当前已输入'+l+'字', f);
		return false;
	}
	
	<?php echo $FD ? $this->fields_func_service->fields_js() : '';?>
	return true;
}
</script>

<script type="text/javascript">
    CKEDITOR.config.filebrowserImageUploadUrl+= '&moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?>&tb=product';
	CKEDITOR.config.filebrowserLinkUploadUrl+='&moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?>&tb=product';
</script>

<script type="text/javascript">
	CKEDITOR.replace('content', {"width":600,"height":200});
</script>

<?php //$this->load->view('footer');?>
</body>
</html>