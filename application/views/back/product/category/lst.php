<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">分类列表</div>
</div>
<div class="bottom">

<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">


<?php 
foreach ($category_arr as $key => $value) {
?>
<tr>
    <td>
  <p style="height:30px;line-height:30px;">
  <?php echo str_repeat('&nbsp;',$value['lev']*4)?>┖─&nbsp;<strong class="cate_list"><?php echo $value['catname'] ?></strong>
  </p>
    </td>
<td align="center">
  分类ID：<?php echo $value['catid'];?>
</td>
<td align="center">&nbsp;</td>
<td align="center">&nbsp;</td>
<td align="center">&nbsp;</td>
<td align="center"><a class="example1" href="<?php echo url('back/product/category/mod');?>&catid=<?php echo $value['catid'];?>"><img src="<?php echo static_url('images/mod.png');?>" alt="编辑"></a></td>
<td align="center"><a href="<?php echo url('back/product/category/del');?>&catid=<?php echo $value['catid'];?>" onclick="return confirm('确定要删除么？有下级分类或有文章的分类无法删除！');"><img src="<?php echo static_url('images/del.png');?>" alt="删除"></a></td>
</tr>
<?php
}
?>
<?php
if(empty($category_arr)){
?>
<tr><td align="center" colspan="7">没有分类</td></tr>
<?php
}
?>
</table>

</div>
</div>

</body>
</html>