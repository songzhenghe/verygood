<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<?php 
load_view('back/cssjs');
?>
<script>

</script>
</head>
<body >

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">文件库</div>
</div>
<div class="bottom">

<form action="" method="get">
<input type="hidden" name="r" value="<?php echo $_GET['r'];?>">
<input type="hidden" name="callback" value="<?php echo $callback;?>">
<input type="hidden" name="moduleid" value="<?php echo $moduleid;?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
    文件名称：<input type="text" name="fileurl" id="" value="<?php echo $fileurl;?>"><br />
    上传时间：<?php echo dcalendar('addtime_1',$addtime_1);?> - <?php echo dcalendar('addtime_2',$addtime_2);?>
    <br />
    <input type="submit" value="搜索" name="submit" class="button"> <input type="button" value="重置" class="button" onclick="location.href='<?php echo smart_url(array('get'=>"moduleid={$moduleid}&callback={$callback}"));?>';"></td>
  </tr>
</table>
</form>
<br />

<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="center">id</td>
    <td align="center">缩略图</td>
    <td align="center">路径</td>
    <td align="center">上传时间</td>
    </tr>
  <?php
  foreach($list as $k=>$v){
  ?>
  <tr>
    <td align="center"><?php echo $v['id'];?></td>
    <td align="center">
    <?php
	$ext=pathinfo(basename($v['fileurl']),PATHINFO_EXTENSION);
    if(in_array($ext,array('jpg','png','gif'))){
		echo "<img src=\"{$v['fileurl']}\" width=\"30\">";
		$type=1;
	}else{
		echo "<img src=\"/common/images/att.png\" width=\"30\">";
		$type=2;
	}
	?>
    </td>
    <td align="center"><a href="javascript:void(0);" onclick="myclick(['<?php echo addslashes($v['fileurl']);?>','<?php echo $type;?>']);"><?php echo $v['fileurl'];?></a></td>
    <td align="center"><?php echo date('Y-m-d H:i:s',$v['addtime']); ?></td>
    </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="4" align="center"><?php echo $pager->fpage();?></td>
  </tr>
</table>

</div>
</div>

<script>
function myclick(arr){
	parent.<?php echo $callback;?>(arr);
}
</script>

</body>
</html>