<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
<?php //$this->load->view('cssjs');?>

<?php
isset($class) or $class='';
isset($i) or $i=0;
?>

<link rel="stylesheet" href="<?php echo VG::$config['base_url'];?>common/css/uploadify.css" type="text/css" />
<script type="text/javascript" src="<?php echo VG::$config['base_url'];?>common/js/swfobject.js"></script>
<script type="text/javascript" src="<?php echo VG::$config['base_url'];?>common/js/jquery.uploadify.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var param={
		'uploader': '<?php echo VG::$config['base_url'];?>common/js/uploadify.swf',
		'cancelImg': '<?php echo VG::$config['base_url'];?>common/images/uploadify/cancel.png',
		'script': '<?php echo $_SERVER['PHP_SELF'].url('back/upload/index/uploadify_files');?>',
		'fileDesc': 'limited file type',
		'fileExt': '*.jpg;*.png;*.gif;*.rar;*.zip;*.pdf;*.txt;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx;*.swf',
		'multi': true,
		'auto': false,
		'sizeLimit': 1024*1024*1024*10,//10M
		'scriptData': {},
		onComplete: function (evt, queueID, fileObj, response, data) {
			var json = eval('(' + response + ')');
			if(parseInt(json.code)==0){
				alert(json.msg);
			}else{
				window.parent.<?php echo $callback;?>([json.url]);	
			}
		}
	};
	$("#fileUploadgrowl").uploadify(param);
});
function _start(){
	
	var s= {'PHPSESSID':'<?php echo session_id();?>','moduleid':'<?php echo $moduleid;?>','uniq':'<?php echo $uniq?>','controller':'<?php echo $controller;?>','tb':'<?php echo $tb;?>','itemid':'<?php echo $itemid;?>','callback':'<?php echo $callback;?>','class':'<?php echo $class;?>','i':'<?php echo $i;?>'};
	$('#fileUploadgrowl').uploadifySettings('scriptData',s,false);
	$('#fileUploadgrowl').uploadifyUpload();
}
function _stop(){
	$('#fileUploadgrowl').uploadifyClearQueue();
}
</script>

</head>
<body>
<?php //$this->load->view('header');?>
<form action="" method="post" id="myform" enctype="multipart/form-data">
<table width="100%" border="1" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="right">文件</td>
    <td align="left">
点击'SELECT FILES'按钮在弹出的窗口中选择要上传的一个或多个文件！单击'开始上传'文件便可上传至服务器！</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td align="center">
    <br />
<br />
<div id="fileUploadgrowl">You have a problem with your javascript</div><br /><br />
		<a href="javascript:_start();">开始上传</a> |  <a href="javascript:_stop();">清除队列</a>
    </td>
  </tr>
  

</table>
</form>

<script>
$(document).ready(function(e) {

	
	$('#myform').get(0).reset();
});
</script>

<?php //$this->load->view('footer');?>
</body>
</html>