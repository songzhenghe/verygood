<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
</head>
<body >
<?php //$this->load->view('header');?>

<br />
<br />
<br />
<br />

<form action="" method="post" enctype="multipart/form-data" id="myform">
<input type="hidden" name="callback" value="<?php echo $callback;?>">
<table width="90%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="right">选择文件：</td>
    <td align="left"><input type="file" name="myfile" id="myfile" class="input" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>

<script>
$(document).ready(function(e) {
    $('#myfile').change(function(){
		$('#myform').submit();
	});
	
	$('#myform').get(0).reset();
});
</script>

<?php //$this->load->view('footer');?>
</body>
</html>