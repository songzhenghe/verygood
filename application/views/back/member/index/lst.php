<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<?php 
load_view('back/cssjs');
?>
<script>
$(document).ready(function(e) {
    $(".example1").colorbox({width:"800px", height:"400px", iframe:true, opacity:0.3});
});
</script>
</head>
<body >

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">会员列表</div>
</div>
<div class="bottom">

<form action="" method="get">
<input type="hidden" name="r" value="<?php echo $_GET['r'];?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
    用户id：<input type="text" name="userid" id="" value="<?php echo $userid;?>"> 
    用户名：<input type="text" name="username" id="" value="<?php echo $username;?>"> 
    所属组：<select name="groupid"><?php echo form_select($groups_select,$groupid);?></select> 
    <input type="submit" value="搜索" name="submit" class="button"> <input type="button" value="重置" class="button" onclick="location.href='<?php echo smart_url();?>';"></td>
  </tr>
</table>
</form>
<br />

<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="center">id</td>
    <td align="center">用户名</td>
    <td align="center">所属组</td>
    <td align="center">操作面板</td>
    <td align="center">后台权限</td>
    <td align="center">编辑</td>
    <td align="center">删除</td>
  </tr>
  <?php
  foreach($list as $k=>$v){
  ?>
  <tr>
    <td align="center"><?php echo $v['userid'];?></td>
    <td align="center"><?php echo $v['username'];?></td>
    <td align="center"><?php echo myswitch($v['groupid'],$groups_select);?></td>
    <td align="center">
    <?php
    if(in_array($v['groupid'],array(2))){
	?>
	<a class="" href="<?php echo url('back/member/website_access/panel',"userid={$v['userid']}");?>"><img src="<?php echo static_url('images/mod.png');?>" alt="操作面板"></a>
    <?php
	}
	?>
    &nbsp;
    </td>
    <td align="center">
    <?php
    if(in_array($v['groupid'],array(2))){
	?>
	<a class="" href="<?php echo url('back/member/website_access/a',"userid={$v['userid']}");?>"><img src="<?php echo static_url('images/mod.png');?>" alt="授权"></a>
    <?php
	}
	?>
    &nbsp;
    </td>
    <td align="center"><a class="" href="<?php echo url('back/member/index/mod',"userid={$v['userid']}");?>"><img src="<?php echo static_url('images/mod.png');?>" alt="编辑"></a></td>
    <td align="center"><a href="<?php echo url('back/member/index/del',"userid={$v['userid']}");?>" onclick="return confirm('确定要删除么？');"><img src="<?php echo static_url('images/del.png');?>" alt="删除"></a></td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="7" align="center"><?php echo $pager->fpage();?></td>
  </tr>
</table>

</div>
</div>


</body>
</html>