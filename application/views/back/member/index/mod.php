<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">编辑会员</div>
</div>
<div class="bottom">

<form action="" method="post" id="myform">
<input type="hidden" name="forward" value="<?php echo VG::$forward;?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="right">用户名：</td>
    <td align="left"><input type="text" name="username" id="" class="input" value="<?php echo $data['username']?>" readonly="readonly" /></td>

  </tr>
  <tr>
    <td align="right">密码：</td>
    <td align="left"><input type="text" name="password" id="" class="input" value="" /></td>
  </tr>
  <tr>
    <td align="right">所属组：</td>
    <td align="left">
    <?php echo form_radio('groupid',$groups_radio,$data['groupid']);?>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>
</div>
</div>

<?php //$this->load->view('footer');?>
</body>
</html>