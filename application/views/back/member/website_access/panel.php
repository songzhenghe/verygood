<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>


</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">操作面板</div>
</div>
<div class="bottom">
<?php

?>
<form action="" method="post" id="myform">

<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">

  <tr>
    <td align="center">操作面板</td>
  </tr>
  <tr>
    <td align="center">
    <div style="margin:0 auto;width:600px;text-align:left;">
	<?php
	foreach($module_list as $k=>$v){
		$menu=$conf->mod_menu($k);
	?>
	<h3><?php echo $v['module_cn'];?> <span style="cursor:pointer;" onClick="javascript:$('input[name=\'url[<?php echo $k;?>][]\']').prop('checked', true);">全选</span>/<span style="cursor:pointer;" onClick="javascript:$('input[name=\'url[<?php echo $k;?>][]\']').prop('checked', false);">取消</span></h3>
    <ol>
        <?php
        foreach($menu as $kk=>$vv){
			if(!$vv[3]) continue;
			if(isset($vv[4]) && !$vv[4]) continue;
        ?>
        <li><label><input <?php if(isset($checked[$k]) && in_array($vv[1],$checked[$k])) echo 'checked="checked"';?> type="checkbox" name="url[<?php echo $k;?>][]" value="<?php echo $vv[1];?>" > <?php echo $vv[0];?> [<?php echo $vv[1];?>]</label></li>
        <?php
        }
        ?>
    </ol>
	<?php
	}
	?>
    </div>
    </td>
  </tr>
 
  <tr>
    <td align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>


</div>
</div>


<?php //$this->load->view('footer');?>
</body>
</html>