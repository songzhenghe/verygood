<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>main.php</title>
<?php load_view('back/cssjs');?>
<style>
._selected{
	text-decoration:underline;
	color:#33C;
	font-weight:bold;
	font-size:14px;	
}
</style>
</head>
<body >
<?php //$this->load->view('header');?>

<div class="container">
<div class="top">
<div class="t_left"></div>
<div class="t_right"></div>
<div class="t_center">后台权限</div>
</div>
<div class="bottom">
<?php

?>
<form action="" method="post" id="myform">
<input type="hidden" name="moduleid" value="<?php echo $moduleid;?>">
<input type="hidden" name="module" value="<?php echo $mod['module_en'];?>">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="mytable">
  <tr>
    <td align="center">
    <?php
    foreach($module_list as $k=>$v){
	?>
    <a href="<?php echo smart_url(array('get'=>"&moduleid={$v['moduleid']}&userid={$userid}"));?>" <?php if($moduleid==$v['moduleid']) echo 'class="_selected"';?>><?php echo $v['moduleid'];?> <?php echo $v['module_cn'];?>[<?php echo $v['module_en'];?>]</a>&nbsp;
    <?php
	}
	?>
    </td>

  </tr>
  <tr>
    <td align="center">操作</td>
  </tr>
  <tr>
    <td align="center">
    <div style="margin:0 auto;width:400px;text-align:left;">
    <?php
    	list($ctl,$act)=$conf->mod_auth($moduleid);
	?>
    <?php 
	foreach($ctl as $k=>$v){
		isset($checked[$v['name']]) or $checked[$v['name']]=array();
		if($v['is_auth']==0) continue;
	?>
    <h3><?php echo $v['chinese'];?>[<?php echo $v['name'];?>] <span style="cursor:pointer;" onClick="javascript:$('input[name=\'access[<?php echo $v['name'];?>][]\']').prop('checked', true);">全选</span>/<span style="cursor:pointer;" onClick="javascript:$('input[name=\'access[<?php echo $v['name'];?>][]\']').prop('checked', false);">取消</span></h3>
    <ol>
    <?php
    foreach($act[$k] as $kk=>$vv){
		if($vv['is_auth']==0) continue;
	?>
    <li><label><input <?php if(in_array($vv['name'],$checked[$v['name']])){ echo 'checked="checked"';}?> style="" type="checkbox" name="access[<?php echo $v['name'];?>][]" value="<?php echo $vv['name'];?>" id=""> <?php echo $vv['chinese'];?>[<?php echo $vv['name'];?>]</label></li>
    <?php
	}
	?>
    </ol>
    <?php
	}
	?>
    </div>
    </td>
  </tr>
  <?php if($flag){?>
  <tr>
    <td align="center">分类</td>
  </tr>
  <tr align="left">
  	<td>
    <input type="hidden" name="flag" value="<?php echo $flag;?>">
    <div style="margin:0 auto;width:400px;" class="cat_list">
    <h3><span style="cursor:pointer;" onClick="javascript:$('input[name=\'category[]\']').prop('checked', true);">全选</span>/<span style="cursor:pointer;" onClick="javascript:$('input[name=\'category[]\']').prop('checked', false);">取消</span></h3>
	<?php
    foreach($cat as $k=>$v){
	?>
	<?php echo str_repeat('&nbsp;',$v['lev']*4)?><input onclick='javascript:checknode(this);' level='<?php echo $v['lev'];?>' <?php if(in_array($v['catid'],$selected)) { echo 'checked="checked"';}?> type="checkbox" name="category[]" id="" value="<?php echo $v['catid'];?>">┖─&nbsp;<?php echo $v['catname'] ?> [<?php echo $v['catid'];?>]<br />
	<?php
	}
	?>
    </div>
    </td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td align="center"><input type="submit" class="button" value="提交" /></td>
    </tr>
</table>
</form>


</div>
</div>
<script type="text/javascript">

  function checknode(obj)
  {
      var chk = $(".cat_list input[type='checkbox']");
      var count = chk.length;
      var num = chk.index(obj);
      var level_top = level_bottom =  chk.eq(num).attr('level')
      for (var i=num; i>=0; i--)
      {
              var le = chk.eq(i).attr('level');
              if(eval(le) < eval(level_top)) 
              {
                  chk.eq(i).prop("checked",true);
                  var level_top = level_top-1;
              }
      }
      for (var j=num+1; j<count; j++)
      {
              var le = chk.eq(j).attr('level');
              if(chk.eq(num).prop("checked")==true) {
                  if(eval(le) > eval(level_bottom)) chk.eq(j).prop("checked",true);
                  else if(eval(le) == eval(level_bottom)) break;
              }
              else {
                  if(eval(le) > eval(level_bottom)) chk.eq(j).prop("checked",false);
                  else if(eval(le) == eval(level_bottom)) break;
              }
      }
  }
</script>

<?php //$this->load->view('footer');?>
</body>
</html>