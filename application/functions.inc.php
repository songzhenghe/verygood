<?php
function mysendmail($subject,$body,$to){
	include_once("./common/phpmailer/class.phpmailer.php");
	$mail = new PHPMailer();     //得到一个PHPMailer实例
	$mail->SetLanguage("zh_cn","./common/phpmailer/language/");
	$body = preg_replace("#[\\\]#i",'',$body);
	$mail->CharSet = "utf-8";  //设置采用utf-8中文编码
	$mail->IsSMTP();                    //设置采用SMTP方式发送邮件
	$mail->Host = VG::$config['mail_host'];    //设置邮件服务器的地址
	$mail->Port = VG::$config['mail_port'];                           //设置邮件服务器的端口，默认为25
	$mail->From     = VG::$config['mail_from'];  //设置发件人的邮箱地址
	$fromname=explode("@",VG::$config['mail_from']);
	$mail->FromName = $fromname[0];                       //设置发件人的姓名
	$mail->SMTPAuth = true;                                    //设置SMTP是否需要密码验证，true表示需要
	$mail->Username=VG::$config['mail_from'];
	$mail->Password = VG::$config['mail_password'];
	$mail->Subject = $subject;                                 //设置邮件的标题
	$mail->AltBody = "text/html";                                // optional, comment out and test
	$mail->MsgHTML($body);
	//$mail->AddAttachment("images/phpmailer.gif");      // 附件       
	$mail->IsHTML(true);                                        //设置内容是否为html类型
	//$mail->WordWrap = 50;                                 //设置每行的字符数
	$mail->AddReplyTo(VG::$config['mail_from'],$fromname[0]);     //设置回复的收件人的地址
	if(is_array($to)){
		for ($i=0,$n=count($to);$i<$n;$i++){
			$toname=explode("@",$to[$i]);
			$mail->AddBCC($to[$i],$toname[0]);     //设置收件的地址			
		}
	}else{
		$toname=explode("@",$to);
		$mail->AddAddress($to,$toname[0]);     //设置收件的地址
	}
	$r=$mail->Send();
	return array($r,$mail->ErrorInfo);
}

function banip($tag,$IP) {
	if(!$tag) return true;
	
	$IP = explode("|", trim($IP));
	$input=new Input();
	$_IP=$input->ip_address();
	$pass = false;
	foreach($IP as $v) {
		if($v == $_IP) { $pass = true; break; }
		if(preg_match("/^".str_replace('*', '[0-9]{1,3}', $v)."$/", $_IP)) { $pass = true; break; }
	}
	if(!$pass) warning('未被允许的IP段');
}

function dhtmlspecialchars($string) {
	if(is_array($string)) {
		return array_map('dhtmlspecialchars', $string);
	} else {
		$string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
		return str_replace('&amp;', '&', $string);
	}
}
function mydtrim($arr){
	if(is_string($arr)){
		$arr=trim($arr);
	}else{
		foreach($arr as $k=>$v){
			$arr[$k]=mydtrim($v);
		}
	}
	return $arr;
}
function cat($catid){
	$ctl=& VG::get_instance();
	$db=$ctl->db;
	return $db->select("select catid,catname,arrparentid,arrchildid,child,item from fv_category where catid='{$catid}'");
}
function cat_pos($CAT) {
	$str = ' &raquo; ';
	$target='';
	$ctl=& VG::get_instance();
	$db=$ctl->db;
	if(!$CAT) return '';
	$arrparentids = $CAT['arrparentid'].','.$CAT['catid'];
	$arrparentid = explode(',', $arrparentids);
	$pos = '';
	$target = $target ? ' target="_blank"' : '';	
	$CATEGORY = array();
	$ddd = $db->mselect("SELECT catid,catname FROM fv_category WHERE catid IN ($arrparentids)");
	
	foreach($ddd as $r){
		$CATEGORY[$r['catid']] = $r;
	}
	$last=count($arrparentid)-1;
	foreach($arrparentid as $index=>$catid) {
		if(!$catid || !isset($CATEGORY[$catid])) continue;
		if($index==$last){
			$pos .= $CATEGORY[$catid]['catname'].$str;
		}else{
			$pos .= $CATEGORY[$catid]['catname'].$str;
		
		}
		
	}
	$_len = strlen($str);
	if($str && substr($pos, -$_len, $_len) === $str) $pos = substr($pos, 0, strlen($pos)-$_len);
	return $pos;
}
function get_area($areaid) {
	$ctl=& VG::get_instance();
	$areaid = intval($areaid);
	return $ctl->db->select("SELECT * FROM fv_area WHERE areaid=$areaid");
}
function area_pos($AREA, $str = ' &raquo; ', $deep = 0) {
	$ctl=& VG::get_instance();
	$db=$ctl->db;
	if(!$AREA) return '';
	$arrparentids = $AREA['arrparentid'].','.$AREA['areaid'];
	$arrparentid = explode(',', $arrparentids);
	
	$AREA = array();
	$ddd = $db->mselect("SELECT areaid,areaname FROM fv_area WHERE areaid IN ($arrparentids)");
	
	foreach($ddd as $r){
		$AREA[$r['areaid']] = $r;
	}
	
	
	$pos = '';
	if($deep) $i = 1;
	foreach($arrparentid as $areaid) {
		if(!$areaid || !isset($AREA[$areaid])) continue;
		if($deep) {
			if($i > $deep) continue;
			$i++;
		}
		$pos .= $AREA[$areaid]['areaname'].$str;
	}
	$_len = strlen($str);
	if($str && substr($pos, -$_len, $_len) === $str) $pos = substr($pos, 0, strlen($pos)-$_len);
	return $pos;
}

function customer_name($itemid){
	$ctl=& VG::get_instance();
	return $ctl->db->get_field("select username from __customer__ where itemid='{$itemid}'");
}
