<?php
class arcModel extends Model{
	public $primaryKey='itemid';
	public $table='article_21';
	
	public function __construct(){
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array();
		$this->_valid_for_add = array();
		$this->_valid_for_mod = array();
		$this->_valid_for_select = array();
		
	}
    public function get_arc(){
        return $this->db->mselect("select * from `vg_article_21`");
    }
}