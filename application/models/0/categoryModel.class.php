<?php
class categoryModel extends Model{
	public $primaryKey='catid';
	public $table='category';
	
	public function __construct(){
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array();
		$this->_valid_for_add = array();
		$this->_valid_for_mod = array();
		$this->_valid_for_select = array();
		
	}
    public function get_cat_list(){
        

		$sql = "SELECT * FROM `$this->full_table` WHERE 1=1";
		$stmt = $this->db->getLink()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		
    }
}