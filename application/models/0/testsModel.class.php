<?php
class testsModel extends Model{
	public $primaryKey='id';
	public $table='tests';
	
	public function __construct(){
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array();
		$this->_valid_for_add = array();
		$this->_valid_for_mod = array();
		$this->_valid_for_select = array();
		
	}

}