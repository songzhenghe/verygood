<?php
class usersModel extends Model{
	public $primaryKey='id';
	public $table='users';
	
	public function __construct(){
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array();
		$this->_valid_for_add = array();
		$this->_valid_for_mod = array();
		$this->_valid_for_select = array();
		
	}
	// public function __call($methodName, $args){
		// p($methodName,$args);
	// }
	public function test_insert(){
		$stmt=$this->db->getLink()->prepare("insert into vg_users (name,age,sex,email) values (?,?,?,?)");  //准备好一个语句
		$result=$stmt->execute(array('0'=>'admin','1'=>'22','2'=>'男','3'=>123));   //执行一个准备好的语句
		return $result;
	}
}