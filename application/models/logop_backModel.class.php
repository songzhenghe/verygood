<?php
class logop_backModel extends Model{
	public $primaryKey='logid';
	public $table='logop_back';
		
	
	public function __construct(){
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array();
		
		$this->_auto_for_add = array();
		$this->_auto_for_mod = array();
		$this->_auto_for_select = array();
		
		$this->_valid_for_add = array();
		$this->_valid_for_mod = array();
		$this->_valid_for_select = array();
	}
	
	public function clear(){
		$today_endtime = strtotime(date('Y-m-d', time()).' 23:59:59');
		$time = $today_endtime - 90*86400;
		$this->db->query("DELETE FROM {$this->full_table} WHERE logtime<$time");
		return true;
	}
	public function record($d){
		//kind,message,module
		$input=new Input();
		$d2=array(
			'username'=>$_SESSION['MY']['USER_NAME'],
			'qstring'=>$_SERVER['REQUEST_URI'],
			'module'=>$d['module'],
			'ip'=>$input->ip_address(),
			'agent'=>$input->user_agent(),
			'logtime'=>time(),
		);
		
		$d=array_merge($d,$d2);

		return $this->easy_insert($d);
	}
}