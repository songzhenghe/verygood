<?php
class login_backModel extends Model{
	public $primaryKey='logid';
	public $table='login_back';
		
	
	public function __construct(){
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array();
		
		$this->_auto_for_add = array();
		$this->_auto_for_mod = array();
		$this->_auto_for_select = array();
		
		$this->_valid_for_add = array();
		$this->_valid_for_mod = array();
		$this->_valid_for_select = array();
		
	}
	public function record($d){
		$input=new Input();
		$d2=array(
			'loginip'=>$input->ip_address(),
			'logintime'=>time(),
			'agent'=>$input->user_agent(),
		);
		$d=array_merge($d,$d2);
		return $this->easy_insert($d);
	}
}