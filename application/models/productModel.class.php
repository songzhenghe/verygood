<?php
class productModel extends Model{
	public $primaryKey='itemid';
	public $table='product';
		
	
	public function __construct(){
		parent::__construct();
	}
	
	public function add($data,$svc){
		isset($data['post_fields']) or $data['post_fields']=array();
		if($svc->FD) $svc->fields_check($data['post_fields']);
		
		foreach($data as $k=>$v){
			if(in_array($k,array('content','post_fields'))) continue;
			$data[$k]=dhtmlspecialchars($v);
		}
		
		$itemid=$this->easy_insert($data);
		if($svc->FD) $svc->fields_update($data['post_fields'], $this->full_table, $itemid);
		return true;
	}
	public function mod($itemid,$data,$svc){
		isset($data['post_fields']) or $data['post_fields']=array();
		if($svc->FD) $svc->fields_check($data['post_fields']);
		
		foreach($data as $k=>$v){
			if(in_array($k,array('content','post_fields'))) continue;
			$data[$k]=dhtmlspecialchars($v);
		}
		
		$this->where(array('itemid'=>$itemid))->easy_update($data);
		if($svc->FD) $svc->fields_update($data['post_fields'], $this->full_table, $itemid);
		return true;
	}
	public function del($id){
		// $r=$this->easy_delete($id);
		// if($r){
			// $uplog_model->clear($this->moduleid,VG::$control,$id);
		// }
	}
	
}