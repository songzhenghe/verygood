<?php
class category_8Model extends category_commonModel{
	public $primaryKey='catid';
	//public $table='category_8';
	
	public function __construct(){
		$this->table=substr(__CLASS__,0,-5);
		parent::__construct();
		
		$this->_auto = array();
		$this->_valid = array(
			array('catname',1,'分类名称不能为空！','require'),
			array('catname',1,'分类名称已存在！','callback','cat_is_exists'),
		);
		$this->_valid_for_add = array(
			0,1
		);
		$this->_valid_for_mod = array(
			0,1
		);
		$this->_valid_for_select = array();
		
	}
	public function get_sub_count($catid,$table=''){
       return parent::get_sub_count($catid,'__product__');
    }
	
}