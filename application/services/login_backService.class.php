<?php
class login_backService extends Service{
	public function __construct(){
		parent::__construct();
	}
	public function count($extra){
		$sql="select count(*) from __login_back__ {$extra}";
		return $this->db->get_field($sql);
	}
	public function lst($extra=''){
		$sql="select * from __login_back__ {$extra}";
		return $this->db->mselect($sql);
	}
}