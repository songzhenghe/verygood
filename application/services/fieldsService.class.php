<?php
class fieldsService extends Service{
	public $itemid;
	public $tb;
	public $pre;
	public $errmsg='error';
	public function __construct(){
		parent::__construct();
		$this->model=load_model('fields');
		$this->pre=VG::$db['dbprefix'];
	}
	
	
	public function cache_fields($moduleid){
		$data=$this->model->where(array('moduleid'=>$moduleid))->order('listorder asc')->easy_select();
		$data=ka_builder($data,'itemid');
		return array_save($data,APPPATH.'runtime/fields_cache/'.$moduleid.'.php');
	}
	public function pass($data) {
		$post=$data['post'];
		
		if(!is_array($post)) return false;
		if(!$post['name']) return $this->_('请填写字段');
		if(!preg_match("/^[a-z0-9]+$/", $post['name'])) return $this->_('字段名只能为小写字母和数字的组合');
		if(!$post['title']) return $this->_('请填写字段名称');
		if(in_array($post['html'], array('select', 'radio', 'checkbox'))) {
			if(!$post['option_value']) return $this->_('请填写选项值');
			if(strpos($post['option_value'], '|') === false) return $this->_('请填写正确的选项值');
		}
		return true;
	}
	public function set($post) {
		if(!in_array($post['html'], array('select', 'radio', 'checkbox'))) {
			$post['option_value'] = '';
		}
		$post['length'] = intval($post['length']);
		if($post['html'] == 'textarea') {
			if($post['type'] != 'varchar' && $post['type'] != 'text') $post['type'] = 'text';
		} else if($post['html'] == 'checkbox' || $post['html'] == 'thumb' || $post['html'] == 'file') {
			$post['type'] = 'varchar';
			$post['length'] = 255;
		} else if($post['html'] == 'editor') {
			$post['type'] = 'text';
		} else if($post['html'] == 'area') {
			$post['type'] = 'int';
			$post['length'] = 10;
		}
		return $post;
	}

	public function get_one() {
		return $this->model->easy_find($this->itemid);
	}

	public function get_list($condition = array(), $order = 'listorder ASC,itemid ASC') {
		$items=$this->model->where($condition)->easy_total();
		$pager=new Page($items);
		$limit_arr=$pager->limit_arr;
		$lists=$this->model->where($condition)->order($order)->limit($limit_arr[0],$limit_arr[1])->easy_select();
		return array($items,$pager->fpage(),$lists);
	}
	public function is_exists($moduleid,$tb,$name,$itemid=0){
		if(!$itemid){
			$item=$this->model->where(array('moduleid'=>$moduleid,'tb'=>$tb,'name'=>$name))->easy_find();
			return $item?false:true;
		}else{
			$item=$this->model->where(array('moduleid'=>$moduleid,'tb'=>$tb,'name'=>$name,'itemid !='=>$itemid))->easy_find();
			return $item?false:true;
		}
	}
	public function add($data) {
		$post = $this->set($data['post']);
		$r=$this->is_exists($post['moduleid'],$post['tb'],$post['name']);
		if(!$r) msg(0,'该字段已经存在');
		$length = 0;
		if($post['type'] == 'varchar') {
			$length = min($post['length'], 255);
		} else if($post['type'] == 'int') {
			$length = min($post['length'], 10);
		}
		$type = strtoupper($post['type']);
		if($length) $type .= "($length)";
		$name = '`'.$post['name'].'`';
        $this->db->query("ALTER TABLE {$this->pre}{$this->tb} ADD $name $type NOT NULL");
		$this->model->easy_insert($post);
		return true;
	}

	public function edit($data) {
		$post = $this->set($data['post']);
		$r=$this->is_exists($post['moduleid'],$post['tb'],$post['name'],$this->itemid);
		if(!$r) msg(0,'该字段已经存在');
		$length = 0;
		if($post['type'] == 'varchar') {
			$length = min($post['length'], 255);
		} else if($post['type'] == 'int') {
			$length = min($post['length'], 10);
		}
		$type = strtoupper($post['type']);
		if($length) $type .= "($length)";
		$cname = '`'.$post['cname'].'`';
		unset($post['cname']);
		$name = '`'.$post['name'].'`';
        $this->db->query("ALTER TABLE {$this->pre}{$this->tb} CHANGE $cname $name $type NOT NULL");
		$this->model->where(array('itemid'=>$this->itemid))->easy_update($post);
		return true;
	}

	public function delete($itemid) {
		$this->itemid = $itemid;
		$r = $this->get_one();
		$name = '`'.$r['name'].'`';
		$this->model->easy_delete($itemid);
	    $this->db->query("ALTER TABLE {$this->pre}{$this->tb} DROP $name");
		return true;
	}
	
	public function order($listorder) {
		if(!is_array($listorder)) return false;
		foreach($listorder as $k=>$v) {
			$k = intval($k);
			$v = intval($v);
			$this->model->where(array('itemid'=>$k))->easy_update(array('listorder'=>$v));
		}
		return true;
	}

	public function _($e) {
		$this->errmsg = $e;
		return false;
	}
	
	
	
}