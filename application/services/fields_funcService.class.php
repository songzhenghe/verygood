<?php
class fields_funcService extends Service{
public $FD=array();
public $moduleid;
public $L;
public $uniq='';
public $itemid=0;
public $controller='';
public $tb='';
public $save_remotepic=0;
public $uplog_model='';

public function __construct(){
	parent::__construct();
	
	$L['fields_input'] = '请填写{V0}';
	$L['fields_choose'] = '请选择{V0}';
	$L['fields_valid'] = '请填写正确的{V0}';
	$L['fields_area'] = '请选择地区';
	$L['fields_less'] = '{V0}不能少于{V1}字符';
	$L['fields_more'] = '{V0}不能多于{V1}字符';
	$L['fields_match'] = '{V0}不符合填写规则';	
	$L['upload'] = '上传';
	$L['preview'] = '预览';
	$L['delete'] = '删除';
	$L['choose'] = '请选择';
	
	
	$this->L=$L;
}

public function fields_read($table=''){
	$file=APPPATH.'runtime/fields_cache/'.$this->moduleid.'.php';
	if(!file_exists($file)) return array();
	if(!$table){
		return include($file);
	}else{
		$fields=include($file);
		$a=array();
		foreach($fields as $k=>$v){
			if($v['tb']==$table) $a[$k]=$v;
		}
		return $a;
	}
}


public function fields_update($post_fields, $table, $itemid, $keyname = 'itemid', $fd = array()) {
	$FD=$this->FD;
	if(!$table || !$itemid) return '';
	if($fd) $FD = $fd;
	$sql = '';
	foreach($FD as $k=>$v) {
		if(isset($post_fields[$v['name']]) || $v['html'] == 'checkbox') {
			$mk = $v['name'];
			$mv = $post_fields[$v['name']];
			if($v['html'] == 'checkbox') $mv = implode(',', $post_fields[$v['name']]);
			//$mv = $v['html'] == 'editor' ? $this->dsafe($mv) : $this->dhtmlspecialchars(trim($mv));
			
			if($v['html'] == 'editor' && $this->save_remotepic && $this->uplog_model && $this->itemid && $this->uniq ){
				$mv=$this->uplog_model->save_remote(array('tb'=>$this->tb,'itemid'=>$this->itemid,'uniq'=>$this->uniq),$mv);
			}
			if($v['html'] == 'editor' && $this->uplog_model && $this->itemid && $this->uniq ){
				$mv=$this->uplog_model->save_local(array('tb'=>$this->tb,'itemid'=>$this->itemid,'uniq'=>$this->uniq),$mv);
			}
			
			$mv = $v['html'] == 'editor' ? $mv : $this->dhtmlspecialchars(trim($mv));
            $mv = $this->db->escape($mv);
			$sql .= ",$mk='$mv'";
		}
	}
	$sql = substr($sql, 1);
	if($sql) $this->db->query("UPDATE {$table} SET $sql WHERE `$keyname`=$itemid");
}

public function fields_check($post_fields, $fd = array()) {
	$FD=$this->FD;
	
	$L=$this->L;
	
	if($fd) $FD = $fd;
	
	$uploads = isset($_SESSION['uploads']) ? $_SESSION['uploads'] : array();
	foreach($FD as $k=>$v) {
		$value = isset($post_fields[$v['name']]) ? $post_fields[$v['name']] : '';
		if(in_array($v['html'], array('thumb', 'file', 'editor')) && $uploads) {
			foreach($uploads as $sk=>$sv) {
				if($v['html'] == 'editor') {
					if(strpos($value, $sv) !== false) unset($_SESSION['uploads'][$sk]);
				} else {
					if($sv == $value) unset($_SESSION['uploads'][$sk]);
				}
			}
		}
		if(!$v['input_limit']) continue;
		if(!defined('CMS_ADMIN') && !$v['front']) continue;
		if($v['input_limit'] == 'is_date') {
			if(!$this->is_date($value)) $this->fields_message($this->lang($L['fields_input'], array($v['title'])));
		} else if($v['input_limit'] == 'is_email') {
			if(!$this->is_email($value)) $this->fields_message($this->lang($L['fields_valid'], array($v['title'])));
		} else if(is_numeric($v['input_limit'])) {
			$length = $v['html'] == 'checkbox' ? count($value) : $this->word_count($value);
			if($length < $v['input_limit']) $this->fields_message($this->lang($L['fields_less'], array($v['title'], $v['input_limit'])));
		} else {
			if(preg_match("/^([0-9]{1,})\-([0-9]{1,})$/", $v['input_limit'], $m)) {			
				$length = $v['html'] == 'checkbox' ? count($value) : $this->word_count($value);
				if($m[1] && $length < $m[1]) $this->fields_message($this->lang($L['fields_less'], array($v['title'], $m[1])));
				if($m[2] && $length > $m[2]) $this->fields_message($this->lang($L['fields_more'], array($v['title'], $m[2])));
			} else {
				if(!preg_match("/^".$v['input_limit']."$/", $value)) $this->fields_message($this->lang($L['fields_match'], array($v['title'])));
			}
		}
	}
}

public function fields_js($fd = array()) {
	$FD=$this->FD;
	if($fd) $FD = $fd;
	$js = '';
	$L=$this->L;
	foreach($FD as $k=>$v) {
		if(!$v['input_limit']) continue;
		if(!defined('CMS_ADMIN') && !$v['front']) continue;
		if($v['input_limit'] == 'is_date') {
			$js .= 'f = "post_fields'.$v['name'].'";l = Dd(f).value.length;';
			$js .= 'if(l != 10) {Dmsg("'.$this->lang($L['fields_input'], array($v['title'])).'", f, 1);return false;}';
		} else if($v['input_limit'] == 'is_email') {
			$js .= 'f = "'.$v['name'].'";l = Dd(f).value.length;';
			$js .= 'if(l < 8) {Dmsg("'.$this->lang($L['fields_input'], array($v['title'])).'", f);return false;}';
		} else if(is_numeric($v['input_limit'])) {
			if($v['html'] == 'area') {
				$js .= 'f = "'.$v['name'].'";l = Dd("areaid_1").value;';
				$js .= 'if(l == 0) {Dmsg("'.$this->lang($L['fields_area']).'", f);return false;}';
			} else {
				$js .= 'f = "'.$v['name'].'";l = Dd(f).value.length;';
				$js .= 'if(l < '.$v['input_limit'].') {Dmsg("'.$this->lang($L['fields_less'], array($v['title'], $v['input_limit'])).'", f);return false;}';
			}
		} else {
			if(preg_match("/^([0-9]{1,})\-([0-9]{1,})$/", $v['input_limit'], $m)) {			
				$js .= 'f = "'.$v['name'].'";l = Dd(f).value.length;';
				if($m[1]) $js .= 'if(l < '.$m[1].') {Dmsg("'.$this->lang($L['fields_less'], array($v['title'], $m[1])).'", f);return false;}';
				if($m[2]) $js .= 'if(l > '.$m[2].') {Dmsg("'.$this->lang($L['fields_more'], array($v['title'], $m[2])).'", f);return false;}';
			} else {
				$js .= 'f = "'.$v['name'].'";l = Dd(f).value;';
				$js .= 'if(l.match(/^'.$v['input_limit'].'$/) == null) {Dmsg("'.$this->lang($L['fields_match'], array($v['title'])).'", f);return false;}';
			}
		}
	}
	return $js;
}
public function fields_array($values=array(),$fd=array()){
    $FD=$this->FD;
	if($fd) $FD = $fd;
    $ret=array();
    foreach($FD as $k=>$v) {
		if(!$v['display']) continue;
		if(!defined('CMS_ADMIN') && !$v['front']) continue;
		$ret[]=$this->fields_k_v($k, $values);
	}
	return $ret;
}

public function fields_k_v($itemid,$values){
    return array($this->FD[$itemid]['title'],$this->FD[$itemid]['name'],$values[$this->FD[$itemid]['name']]);
}

public function fields_html($left = '<td align="right">', $right = '<td>', $values = array(), $fd = array()) {
	//extract($GLOBALS, EXTR_SKIP);
	$FD=$this->FD;
	if($fd) $FD = $fd;
	$html = '';
	foreach($FD as $k=>$v) {
		if(!$v['display']) continue;
		if(!defined('CMS_ADMIN') && !$v['front']) continue;
		$html .= $this->fields_show($k, $left, $right, $values, $fd);
	}
	return $html;
}

public function fields_show($itemid, $left = '<td align="right">', $right = '<td>', $values = array(), $fd = array()) {
	//extract($GLOBALS, EXTR_SKIP);
	static $thumb_index=0;
	static $file_index=0;
	static $editor_index=0;
	$L=$this->L;
	$FD=$this->FD;
	if($fd) $FD = $fd;
	if(!$values) {
		if(isset($item)) $values = $item;
		if(isset($user)) $values = $user;
	}
	$html = '';
	$v = $FD[$itemid];
	$value = $v['default_value'];
	if(isset($values[$v['name']])) {
		$value = $values[$v['name']];
	} else if($v['default_value']) {
		eval('$value = "'.$v['default_value'].'";');
	}
	if($v['html'] == 'hidden') {
		$html .= '<input type="hidden" name="post_fields['.$v['name'].']" id="'.$v['name'].'" value="'.$value.'" '.$v['addition'].'/>';
	} else {
		$html .= '<tr>'.$left;
		if($v['input_limit']) {
			$html .= '<span class="f_red">*</span> ';
		} else {
			$html .= defined('CMS_ADMIN') ? '<span class="f_hid">*</span> ' : '';
		}
		$html .= $v['title'];
		$html .= '</td>';
		$html .= $right;
		switch($v['html']) {
			case 'text':
				$html .= '<input type="text" name="post_fields['.$v['name'].']" id="'.$v['name'].'" value="'.$value.'" '.$v['addition'].'/> <span class="f_red" id="d'.$v['name'].'"></span>';
			break;
			case 'textarea':
				$html .= '<textarea name="post_fields['.$v['name'].']" id="'.$v['name'].'" '.$v['addition'].'>'.$value.'</textarea> <span class="f_red" id="d'.$v['name'].'"></span>';
			break;
			case 'select':
				if($v['option_value']) {
					$html .= '<select name="post_fields['.$v['name'].']" id="'.$v['name'].'" '.$v['addition'].'><option value="">'.$L['choose'].'</option>';
					$rows = explode("*", $v['option_value']);
					foreach($rows as $row) {
						if($row) {
							$cols = explode("|", trim($row));
							$html .= '<option value="'.$cols[0].'"'.($cols[0] == $value ? ' selected' : '').'>'.$cols[1].'</option>';
						}
					}
					$html .= '</select> <span class="f_red" id="d'.$v['name'].'"></span>';
				}
			break;
			case 'radio':
				if($v['option_value']) {
					$html .= '<span id="'.$v['name'].'">';
					$rows = explode("*", $v['option_value']);
					foreach($rows as $rw => $row) {
						if($row) {
							$cols = explode("|", trim($row));
							$html .= '<input type="radio" name="post_fields['.$v['name'].']" value="'.$cols[0].'" id="'.$v['name'].'_'.$rw.'"'.($cols[0] == $value ? ' checked' : '').'> '.$cols[1].'&nbsp;&nbsp;&nbsp;';
						}
					}
					$html .= '</span> <span class="f_red" id="d'.$v['name'].'"></span>';
				}
			break;
			case 'checkbox':
				if($v['option_value']) {
					$html .= '<span id="'.$v['name'].'">';
					$value = explode(',', $value);
					$rows = explode("*", $v['option_value']);
					foreach($rows as $rw => $row) {
						if($row) {
							$cols = explode("|", trim($row));
							$html .= '<input type="checkbox" name="post_fields['.$v['name'].'][]" value="'.$cols[0].'" id="'.$v['name'].'_'.$rw.'"'.(in_array($cols[0], $value) ? ' checked' : '').'> '.$cols[1].'&nbsp;&nbsp;&nbsp;';
						}
					}
					$html .= '</span> <span class="f_red" id="d'.$v['name'].'"></span>';
				}
			break;
			case 'date':
				$html .= dcalendar('post_fields['.$v['name'].']', $value);
				$html .= ' <span class="f_red" id="dpost_fields'.$v['name'].'"></span>';
			break;
			case 'thumb':
			$u=url("back/upload/index/up_img","callback=danger_fill_thumb&class=danger_img&i={$thumb_index}&moduleid={$this->moduleid}&uniq={$this->uniq}&itemid={$this->itemid}&controller={$this->controller}&tb={$this->tb}");
            /*
            &moduleid=<?php echo $this->moduleid;?>&uniq=<?php echo $this->_UNIQ;?>&itemid=<?php echo intval($itemid);?>&controller=<?php echo VG::$control;?
            */
			$thumb_index++;
$html .=<<<st
<input type="text" size="60" name="post_fields[{$v['name']}]" id="{$v['name']}" value="{$value}" {$v['addition']} class="input danger_img">[<a href="{$u}" class="danger_thumb">上传</a>]&nbsp;[<a class="danger_thumb_preview" href="#">预览</a>]&nbsp;[<a href="#" class="danger_thumb_clear">清空</a>]
st;
$html .= ' <span class="f_red" id="d'.$v['name'].'"></span>';
			
			
			
				// $html .= '<input name="post_fields['.$v['name'].']" type="text" size="60" id="'.$v['name'].'" value="'.$value.'" '.$v['addition'].'/>&nbsp;&nbsp;<span onclick="Dthumb('.$moduleid.','.$v['width'].','.$v['height'].', Dd(\''.$v['name'].'\').value,\''.(defined('CMS_ADMIN') ? '' : '1').'\',\''.$v['name'].'\');" class="jt">['.$L['upload'].']</span>&nbsp;&nbsp;<span onclick="_preview(Dd(\''.$v['name'].'\').value);" class="jt">['.$L['preview'].']</span>&nbsp;&nbsp;<span onclick="Dd(\''.$v['name'].'\').value=\'\';" class="jt">['.$L['delete'].']</span>';
				// $html .= ' <span class="f_red" id="d'.$v['name'].'"></span>';
			break;
			case 'file':
			$u=url("back/upload/index/up_file","callback=danger_fill_file&class=danger_f&i={$file_index}&moduleid={$this->moduleid}&uniq={$this->uniq}&itemid={$this->itemid}&controller={$this->controller}&tb={$this->tb}");
			$file_index++;
$html .=<<<st
<input type="text" size="60" name="post_fields[{$v['name']}]" id="{$v['name']}" value="{$value}" {$v['addition']} class="input danger_f">[<a href="{$u}" class="danger_file">上传</a>]&nbsp;[<a class="danger_file_preview" href="#">预览</a>]&nbsp;[<a href="#" class="danger_file_clear">清空</a>]
st;
$html .= ' <span class="f_red" id="d'.$v['name'].'"></span>';
				// $html .= '<input name="post_fields['.$v['name'].']" type="text" size="60" id="'.$v['name'].'" value="'.$value.'" '.$v['addition'].'/>&nbsp;&nbsp;<span onclick="Dfile('.$moduleid.', Dd(\''.$v['name'].'\').value, \''.$v['name'].'\');" class="jt">['.$L['upload'].']</span>&nbsp;&nbsp;<span onclick="if(Dd(\''.$v['name'].'\').value) window.open(Dd(\''.$v['name'].'\').value);" class="jt">['.$L['preview'].']</span>';
				// $html .= ' <span class="f_red" id="d'.$v['name'].'"></span>&nbsp;&nbsp;<span onclick="Dd(\''.$v['name'].'\').value=\'\';" class="jt">['.$L['delete'].']</span>';
				// $html .= ' <span class="f_red" id="d'.$v['name'].'"></span>';
			break;
			case 'editor':
if($editor_index==0){
$html.=<<<st
<!--<script type="text/javascript" src="/common/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript">
	window.CKEDITOR_BASEPATH='/common/ckeditor/';
}
</script>-->
st;
}
$html .=<<<st
<textarea name="post_fields[{$v['name']}]" id="{$v['name']}" cols="50" rows="5">{$value}</textarea>
<script type="text/javascript">
CKEDITOR.replace('{$v['name']}', {"width":{$v['width']},"height":{$v['height']}});
</script>
st;
$html .= '<br/><span class="f_red" id="d'.$v['name'].'"></span>';

$obj_uniq=rand(100000,999999);
$uu=url("back/upload/index/uploadify_files","callback=callback_bit_file_up_{$obj_uniq}");
$uu2=url("back/upload/index/files_browser","callback=callback_files_browser_{$obj_uniq}");
$uu3=url("back/editor_help/word/index","editor={$v['name']}");
$html .=<<<st
      <br />
	[<a href="{$uu}&moduleid={$this->moduleid}&uniq={$this->uniq}&itemid={$this->itemid}&controller={$this->controller}&tb={$this->tb}" class="bit_file_up">多文件上传</a>]&nbsp;[<a href="{$uu2}&moduleid={$this->moduleid}" class="files_browser">文件库</a>]&nbsp;[<a href="{$uu3}&moduleid={$this->moduleid}&uniq={$this->uniq}&itemid={$this->itemid}&controller={$this->controller}&tb={$this->tb}" class="editor_help">编辑助手</a>]
<ul id="target_bit_file_up_{$obj_uniq}"></ul>
<script>
function callback_bit_file_up_{$obj_uniq}(arr){
	//[path]
	var ext=arr[0].substring(arr[0].lastIndexOf("."),arr[0].length).toLowerCase();//后缀名
	if(ext=='.jpg' || ext=='.png' || ext=='.gif'){
		var thumb="<img src=\""+arr[0]+"\" width=\"30\">";	
	}else{
		var thumb="<img src=\"/common/images/att.png\" width=\"30\">";	
	}
	var html1="[<a href=\"javascript:void(0);\" onclick=\"_insert_pic('{$v['name']}','"+arr[0]+"');\">以图片插入</a>] ";
	var html2="[<a href=\"javascript:void(0);\" onclick=\"_insert_att('{$v['name']}','"+arr[0]+"');\">以附件插入</a>] ";
	$("#target_bit_file_up_{$obj_uniq}").append("<li>"+thumb+" "+html1+html2+"</li>");
	return true;
}
function callback_files_browser_{$obj_uniq}(arr){
	if(parseInt(arr[1])==1){
		_insert_pic('{$v['name']}',arr[0]);
	}else{
		_insert_att('{$v['name']}',arr[0]);
	}
	return true;
}
</script>


st;

$editor_index++;
				/*
				$toolbar = isset($group_editor) ? $group_editor : 'Destoon';
				if(DT_EDITOR == 'fckeditor') {
					$html .= '<textarea name="post_fields['.$v['name'].']" id="'.$v['name'].'" style="display:none">'.$value.'</textarea><iframe id="'.$v['name'].'___Frame" src="'.$MODULE[2]['linkurl'].'editor/fckeditor/editor/fckeditor.html?InstanceName='.$v['name'].'&Toolbar='.$toolbar.'" width="'.$v['width'].'" height="'.$v['height'].'" frameborder="no" scrolling="no"></iframe>';
					$html .= '<br/><span class="f_red" id="d'.$v['name'].'"></span>';
				} else {
					$html .= '<textarea name="post_fields['.$v['name'].']" id="'.$v['name'].'" style="display:none">'.$value.'</textarea>'. deditor($moduleid, $v['name'], $toolbar, $v['width'], $v['height']).'<span class="f_red" id="d'.$v['name'].'"></span>';
				}
				*/
			break;
			case 'area':
				$html .= ajax_area_select('post_fields['.$v['name'].']', $L['choose'], $value);
				$html .= ' <span class="f_red" id="d'.$v['name'].'"></span>';
			break;
		}
		$html .= $v['note'];
		$html .= '</td></tr>';
	}
	return $html;
}

public function fields_message($msg) {
	msg(0,$msg);
}

protected function strip_sql($string, $type = 1) {
	$match = array("/union/i","/where/i","/having/i","/outfile/i","/dumpfile/i","/0x([a-f0-9]{2,})/i","/select([\s\S]*?)from/i","/select([\s\*\/\-\{\(\+@`])/i","/update([\s\*\/\-\{\(\+@`])/i","/replace([\s\*\/\-\{\(\+@`])/i","/delete([\s\*\/\-\{\(\+@`])/i","/drop([\s\*\/\-\{\(\+@`])/i","/load_file[\s]*\(/i","/substring[\s]*\(/i","/substr[\s]*\(/i","/left[\s]*\(/i","/right[\s]*\(/i","/mid[\s]*\(/i","/concat[\s]*\(/i","/concat_ws[\s]*\(/i","/make_set[\s]*\(/i","/ascii[\s]*\(/i","/bin[\s]*\(/i","/oct[\s]*\(/i","/hex[\s]*\(/i","/ord[\s]*\(/i","/char[\s]*\(/i","/conv[\s]*\(/i");
	$replace = array('unio&#110;','wher&#101;','havin&#103;','outfil&#101;','dumpfil&#101;','0&#120;\\1','selec&#116;\\1fro&#109;','selec&#116;\\1','updat&#101;\\1','replac&#101;\\1','delet&#101;\\1','dro&#112;\\1','load_fil&#101;(','substrin&#103;(','subst&#114;(','lef&#116;(','righ&#116;(','mi&#100;(','conca&#116;(','concat_w&#115;(','make_se&#116;(','asci&#105;(','bi&#110;(','oc&#116;(','he&#120;(','or&#100;(','cha&#114;(','con&#118;(');
	if($type) {
		return is_array($string) ? array_map(array($this,'strip_sql'), $string) : preg_replace($match, $replace, $string);
	} else {
		return str_replace(array('&#100;', '&#101;', '&#103;', '&#105;', '&#109;', '&#110;','&#112;', '&#114;', '&#115;', '&#116;', '&#118;', '&#120;'), array('d', 'e', 'g', 'i', 'm', 'n', 'p', 'r', 's', 't', 'v', 'x'), $string);
	}
}
protected function dsafe($string, $type = 1) {
	if(is_array($string)) {
		return array_map(array($this,'dsafe'), $string);
	} else {
		$string = preg_replace("/\<\!\-\-([\s\S]*?)\-\-\>/", "", $string);
		$string = preg_replace("/\/\*([\s\S]*?)\*\//", "", $string);
		$string = preg_replace("/&#([a-z0-9]+)([;]*)/i", "", $string);
		if(preg_match("/&#([a-z0-9]+)([;]*)/i", $string)) return nl2br(strip_tags($string));
		$match = array("/s[\s]*c[\s]*r[\s]*i[\s]*p[\s]*t/i","/d[\s]*a[\s]*t[\s]*a[\s]*\:/i","/b[\s]*a[\s]*s[\s]*e/i","/e[\\\]*x[\\\]*p[\\\]*r[\\\]*e[\\\]*s[\\\]*s[\\\]*i[\\\]*o[\\\]*n/i","/i[\\\]*m[\\\]*p[\\\]*o[\\\]*r[\\\]*t/i","/on([a-z]{2,})([\(|\=|\s]+)/i","/about/i","/frame/i","/link/i","/meta/i","/textarea/i","/eval/i","/alert/i","/confirm/i","/prompt/i","/cookie/i","/document/i","/newline/i","/colon/i","/<style/i","/\\\x/i");
		$replace = array("s<em></em>cript","da<em></em>ta:","ba<em></em>se","ex<em></em>pression","im<em></em>port","o<em></em>n\\1\\2","a<em></em>bout","f<em></em>rame","l<em></em>ink","me<em></em>ta","text<em></em>area","e<em></em>val","a<em></em>lert","/con<em></em>firm/i","prom<em></em>pt","coo<em></em>kie","docu<em></em>ment","new<em></em>line","co<em></em>lon","<sty1e","\<em></em>x");
		if($type) {
			return str_replace(array('isShowa<em></em>bout'), array('isShowAbout'), preg_replace($match, $replace, $string));
		} else {
			return str_replace(array('<em></em>', '<sty1e'), array('', '<style'), $string);
		}
	}
}
protected function dhtmlspecialchars($string) {
	if(is_array($string)) {
		return array_map(array($this,'dhtmlspecialchars'), $string);
	} else {
		$string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
		$string = str_replace('&amp;', '&', $string);
		if(defined('CMS_ADMIN')) return $string;
		$_string = str_replace(array('&quot;', '&#34;', '"'), array('', '', ''), $string);
		if($_string == $string) return $string;
		// return $this->strip_sql($_string);
		return $_string;
	}
}
protected function word_count($string){
	return mb_strlen($string, 'UTF-8');
}
protected function lang($str, $arr = array()) {
	if($arr) {
		foreach($arr as $k=>$v) {
			$str = str_replace('{V'.$k.'}', $v, $str);
		}
	}
	return $str;
}
protected function is_email($email) {
	return strlen($email) > 6 && preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $email);
}
protected function is_date($date, $sep = '-') {
	if(strlen($date) == 8) $date = substr($date, 0, 4).'-'.substr($date, 4, 2).'-'.substr($date, 6, 2);
	if(strlen($date) > 10 || strlen($date) < 8)  return false;
	list($year, $month, $day) = explode($sep, $date);
	return checkdate($month, $day, $year);
}
	
}