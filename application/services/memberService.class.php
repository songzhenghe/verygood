<?php
class memberService extends Service{
	public function __construct(){
		parent::__construct();
	}
	public function count($extra){
		$sql="select count(*) from __member__ {$extra}";
		return $this->db->get_field($sql);
	}
	public function lst($extra=''){
		$sql="select * from __member__ {$extra}";
		return $this->db->mselect($sql);
	}
	public function panel($userid,$post){
		$model=load_model('panel_back');
		isset($post['url']) or $post['url']=array();
		$model->where(array('userid'=>$userid))->easy_delete();
		
		foreach($post['url'] as $moduleid=>$url){
			foreach($url as $u){
				$model->easy_insert(array('userid'=>$userid,'moduleid'=>$moduleid,'url'=>$u));
			}
		}
		return true;
	}
	
	public function get_panel($userid){

		$model=load_model('panel_back');
		$data=$model->where(array('userid'=>$userid))->easy_select();
		$return=array();
		foreach($data as $k=>$v){
			$return[$v['moduleid']][]=$v['url'];
		}
		
		return array($return);
	}
	//后台权限
	public function a($userid,$post){
		$model=load_model('access_back');
		isset($post['access']) or $post['access']=array();
		$model->where(array('userid'=>$userid,'moduleid'=>$post['moduleid']))->easy_delete();
		
		foreach($post['access'] as $controller=>$actions){
			foreach($actions as $action){
				$model->easy_insert(array('userid'=>$userid,'moduleid'=>$post['moduleid'],'module'=>$post['module'],'controller'=>$controller,'action'=>$action));
			}
		}
		
		if(!$post['flag']) return true;
		
		$model=load_model('cat_back');
		isset($post['category']) or $post['category']=array(0);
		$model->where(array('userid'=>$userid,'moduleid'=>$post['moduleid']))->easy_delete();
		
		$catid_str=implode(',',$post['category']);
		
		$model->easy_insert(array('userid'=>$userid,'moduleid'=>$post['moduleid'],'catid'=>$catid_str));
		
		return true;
	}
	public function get_a($userid,$moduleid){
		$model=load_model('access_back');
		$data=$model->where(array('userid'=>$userid,'moduleid'=>$moduleid))->easy_select();
		$return=array();
		foreach($data as $k=>$v){
			$return[$v['controller']][]=$v['action'];
		}
		
		$model=load_model('cat_back');
		$data=$model->where(array('userid'=>$userid,'moduleid'=>$moduleid))->easy_find();
		$return2=array(0);
		if($data['catid']){
			$return2=array_merge($return2,explode(',',$data['catid']));
		}
		
		return array($return,$return2);
	}
}