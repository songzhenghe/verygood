<?php
class logop_backService extends Service{
	public function __construct(){
		parent::__construct();
	}
	public function count($extra){
		$sql="select count(*) from __logop_back__ {$extra}";
		return $this->db->get_field($sql);
	}
	public function lst($extra=''){
		$sql="select * from __logop_back__ {$extra}";
		return $this->db->mselect($sql);
	}
}